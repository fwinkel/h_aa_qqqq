# GNN tagger
Here we do the classification of dicharm jets vs QCD jets. 

### taggerInputMaker.C
Convert the nTupleMaker outputs to rootfiles with the neccessary inputs to the gnn (In the future, h5 conversion should be implemented)
To run:

 1. Run nTupleMaker and produce signal and background rootfiles.
 2. Write path to signal and background rootfiles in inputMaker(), inside main/taggerInputMaker. 	
 3. ```root -l taggerInputMaker.C```
 4. It will produce separate rootfiles for signal and background and hadd them into a file called ```inputs.root ```.
 5. WARNING: The shuffling of the events will be performed in the gnnTagger.py preprocessing.

### gnnTagger.py
pytorch based GNN. To run:
 1. Give ```inputs.root``` path to ```fileName```
 2. Run ```python3 gnnTagger.py```
 3. It will shuffle the inputs, separate into train and test samples, preprocess them and train a GNN.
 4. Also will produce loss vs epochs plot, scoring plot and ROC curve.

### preprocessing.py
Contains functions that convert input arrays into torch tensors, manipulate graph indices and creates batches for training
