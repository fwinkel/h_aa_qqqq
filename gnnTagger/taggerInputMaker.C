#include <iostream>
#include <string>
#include <cstdlib> // Necesario para exit() y system()
#include <ROOT/RDataFrame.hxx>
#include <ROOT/RVec.hxx>
using namespace std;
using namespace ROOT;

float DeltaR(float eta1,float phi1,float eta2,float phi2)
{
  float deltaPhi = TMath::Abs(phi1-phi2);
  float deltaEta = eta1-eta2;
  if(deltaPhi > TMath::Pi()){ deltaPhi = TMath::TwoPi() - deltaPhi;}
  return TMath::Sqrt(deltaEta*deltaEta + deltaPhi*deltaPhi);
}

void inputMaker(const char* inputFileName, const char* outputFileName){

  // Generate dictionary for nested vectors	
  gInterpreter->GenerateDictionary("ROOT::VecOps::RVec<std::vector<float>>", "ROOT/RVec.hxx");
	 
  // Tree name 
  auto treeName = "analysis";

  cout<<"Reading input file "<<inputFileName<<endl; 
  
  // Read tree as RDataFrame 
  RDataFrame df(treeName, inputFileName);

  cout<<"======================================================== "<<endl;
  cout<<"                                                         "<<endl;
  cout<<"                Adding new branches                      "<<endl;
  cout<<"                                                         "<<endl;
  cout<<"======================================================== "<<endl;

  //std::cout << "Entries before filter: " << df.Count().GetValue() << std::endl;
  //auto df0 = df.Filter("CaloJet_pt.size() == nTracks.size()");
  //std::cout << "Entries after filter: " << df0.Count().GetValue() << std::endl;

  auto df1 = df.Define("TrueLabel", "CaloJet_TruthID==44") // cc-jets      
  //auto df1 = df.Define("TrueLabel", "CaloJet_TruthID==4")   // c-jets   
               .Define("mask", "TrueLabel==1")
               .Define("_CaloJet_pt",          "CaloJet_pt[mask]" )                           
               .Define("_CaloJet_eta",         "CaloJet_eta[mask]")                      
               //.Define("_CaloJet_phi",         "CaloJet_phi[mask]")                   
               .Define("_TrueLabel",           "TrueLabel[mask]"  )                    
               .Define("_nTracks",             "nTracks[mask]"    )                       
               
               //.Define("_Tracks_eta",          "Tracks_eta[mask]") 
               //.Define("_Tracks_phi",          "Tracks_phi[mask]")                   
               //.Define("_Tracks_pt",           "Tracks_pt[mask]")                      
               //.Define("_Tracks_e",            "Tracks_e[mask]")                    
               .Define("_Tracks_d0",                          "Tracks_d0[mask]"                         )           
               .Define("_Tracks_z0SinTheta",                  "Tracks_z0SinTheta[mask]"                 )           
               .Define("_Tracks_deta",                        "Tracks_deta[mask]"                       )           
               .Define("_Tracks_dphi",                        "Tracks_dphi[mask]"                       )           
               .Define("_Tracks_qOverP",                      "Tracks_qOverP[mask]"                     )             
               .Define("_Tracks_IP3D_signed_d0_significance", "Tracks_IP3D_signed_d0_significance[mask]")             
               .Define("_Tracks_IP3D_signed_z0_significance", "Tracks_IP3D_signed_z0_significance[mask]")             
               .Define("_Tracks_phiUncert",                   "Tracks_phiUncert[mask]"                  ) 
               .Define("_Tracks_thetaUncert",                 "Tracks_thetaUncert[mask]"                ) 
               .Define("_Tracks_qOverPUncert",                "Tracks_qOverPUncert[mask]"               ) 
               .Define("_NumPixHits",                         "NumPixHits[mask]"                        )          
               .Define("_NumSCTHits",                         "NumSCTHits[mask]"                        )          
               .Define("_NumIPLHits",                         "NumIPLHits[mask]"                        )          
               .Define("_NumNIPLHits",                        "NumNIPLHits[mask]"                       )          
               .Define("_NumIPLSharedHits",                   "NumIPLSharedHits[mask]"                  ) 
               .Define("_NumIPLSplitHits",                    "NumIPLSplitHits[mask]"                   ) 
               .Define("_NumPixSharedHits",                   "NumPixSharedHits[mask]"                  )    
               .Define("_NumPixSplitHits",                    "NumPixSplitHits[mask]"                   )    
               .Define("_NumSCTSharedHits",                   "NumSCTSharedHits[mask]"                  )    
               .Define("_NumPixHoles",                        "NumPixHoles[mask]"                       )         
               .Define("_NumSCTHoles",                        "NumSCTHoles[mask]"                       )         
               .Define("_Tracks_leptonID",                    "Tracks_leptonID[mask]"                   );         


  auto df2 = df1.Define(
        "NumberOfEdges",
        [](
                const ROOT::RVec<int>& jets_nTracks
        ) -> ROOT::RVec<int> {
                ROOT::RVec<int> ret;
                for(size_t i=0; i<jets_nTracks.size(); i++) {
                  int nTracks = jets_nTracks.at(i);
                  int nEdges  = (nTracks*(nTracks-1))/2;
                  ret.push_back(nEdges);
                }
                return ret;
        },
        { "_nTracks" }
  );

  // Add column to DataFrame with the indexes of the reciever nodes
  auto df3 = df2.Define(
        "Tracks_recieverI",
        [](
                const ROOT::RVec<int>& jets_nTracks
        ) -> ROOT::RVec<int> {
                ROOT::RVec<int> ret;
                for(size_t i=0; i<jets_nTracks.size(); i++) {
                  int nTracks = jets_nTracks.at(i);
                  for (int j=0; j<nTracks; j++){
                    for (int k=j+1; k<nTracks; k++){
                      ret.push_back(j);
                    }
                  }
                }
                return ret;
        },
        { "_nTracks" }
  );

  // Add column to DataFrame with the indexes of the sender nodes
  auto df4 = df3.Define(
        "Tracks_senderI",
        [](
                const ROOT::RVec<int>& jets_nTracks
        ) -> ROOT::RVec<int> {
                ROOT::RVec<int> ret;
                for(size_t i=0; i<jets_nTracks.size(); i++) {
                  int nTracks = jets_nTracks.at(i);
                  for (int j=0; j<nTracks; j++){
                    for (int k=j+1; k<nTracks; k++){
                      if (j!=k) {ret.push_back(k);}
                    }
                  }
                }
                return ret;
        },
        { "_nTracks" }
  );
  // Add column to DataFrame with edge features

//  auto df6 = df4.Define(
//        "Tracks_dR",
//        [](
//        	const ROOT::RVec<int>&   jets_nTracks,
//        	const ROOT::RVec<float>& tracks_eta,
//        	const ROOT::RVec<float>& tracks_phi
//        ) -> ROOT::RVec<float> {
//                ROOT::RVec<float> ret;
//                int firstIndex = 0;
//		int lastIndex  = 0;
//		// Loop on vector of number of tracks
//		for (int n : jets_nTracks){
//		  // Loop on tracks associated to each jet
//		  for (int i=firstIndex; i<n+lastIndex; i++){
//		    //for (int j=i+1; j<n+lastIndex; j++){
//		    for (int j=firstIndex; j<n+lastIndex; j++){
//		      // Calculate dR between tracks *that belong to a single jet*
//		      float deltaR = DeltaR(tracks_eta[i], tracks_phi[i], tracks_eta[j], tracks_phi[j]);
//		      if (j!=i){ret.push_back(deltaR);}
//		    }
//		  }
//		  firstIndex=n+lastIndex;
//		  lastIndex+=n;
//		}
//                return ret;
//        },
//        { "nTracks", "Tracks_eta", "Tracks_phi" }
//  );

  auto df7 = df4.Define(
        "EdgeSenderI",
        [](
        	const ROOT::RVec<int>&   jets_nTracks
        ) -> ROOT::RVec<int> {
                ROOT::RVec<int> ret;
		// Loop on vector of number of tracks
		for (int n : jets_nTracks){
		  // Loop on tracks associated to each jet
		  for (int i=0; i<n; i++){
		    for (int j=0; j<n; j++){
		      if (j!=i){ret.push_back(i);}
		    }
		  }
		}
                return ret;
        },
        { "_nTracks" }
  );

  auto df8 = df7.Define(
        "EdgeRecieverI",
        [](
        	const ROOT::RVec<int>&   jets_nTracks
        ) -> ROOT::RVec<int> {
                ROOT::RVec<int> ret;
		// Loop on vector of number of tracks
		for (int n : jets_nTracks){
		  // Loop on tracks associated to each jet
		  for (int i=0; i<n; i++){
		    for (int j=0; j<n; j++){
		      if (j!=i){ret.push_back(j);}
		    }
		  }
		}
                return ret;
        },
        { "_nTracks" }
  );

  auto df9 = df8.Define(
        "NumberOfUnions",
        [](
        	const ROOT::RVec<int>&   jets_nTracks
        ) -> ROOT::RVec<int> {
                ROOT::RVec<int> ret;
		// Loop on vector of number of tracks
		for (int n : jets_nTracks){
		  int count = 0;
		  // Loop on tracks associated to each jet
		  for (int i=0; i<n; i++){
		    for (int j=0; j<n; j++){
		      if (j!=i){
		        count+=1;
		      }
		    }                                       
		  }
		  ret.push_back(count);
		}
                return ret;
        },
        { "_nTracks" }
  );

  ////////////////////////////////////////////////////////////////////////////////////////////////////////
  ////////////////////// Create output file //////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////////////////////////////////

  // List of branches used for inputs
  vector<string> branchList = {"_CaloJet_pt",
                               "_CaloJet_eta", 
                               //"_CaloJet_phi", 
                               "_TrueLabel",
                               "_nTracks",          
                               //"_Tracks_eta",          
                               //"_Tracks_phi",          
                               //"_Tracks_pt",           
                               //"_Tracks_e",            
                               "_Tracks_d0",                         
                               "_Tracks_z0SinTheta",                 
                               "_Tracks_deta",                       
                               "_Tracks_dphi",                       
                               "_Tracks_qOverP",                     
                               "_Tracks_IP3D_signed_d0_significance",
                               "_Tracks_IP3D_signed_z0_significance",
                               "_Tracks_phiUncert",                  
                               "_Tracks_thetaUncert",                
                               "_Tracks_qOverPUncert",               
                               "_NumPixHits",                        
                               "_NumSCTHits",                        
                               "_NumIPLHits",                        
                               "_NumNIPLHits",                       
                               "_NumIPLSharedHits",                  
                               "_NumIPLSplitHits",                   
                               "_NumPixSharedHits",                  
                               "_NumPixSplitHits",                   
                               "_NumSCTSharedHits",                  
                               "_NumPixHoles",                       
                               "_NumSCTHoles",                       
                               "_Tracks_leptonID",                   
                               ////"_MCEventWeight",
                               "NumberOfEdges",
                               "Tracks_recieverI",
                               "Tracks_senderI",
                               "EdgeSenderI",
                               "EdgeRecieverI",
                               "NumberOfUnions"
                              };
  
  // Make new tree 
  df9.Snapshot(treeName, outputFileName, branchList);

  cout<<"New file generated: "<<outputFileName<<endl;
  cout<<"======================================================== "<<endl;
  
}

int taggerInputMaker(){

  inputMaker("/eos/home-f/fwinkel/imGoingCrazy/trees/signal.root", "./signal_skimmed.root");                 	
  //inputMaker("/eos/home-f/fwinkel/imGoingCrazy/trees/background.root", "./background_skimmed.root");                 	

  //const char* haddFiles = "hadd inputs_test.root mc20_13TeV_tracksGraphed.root background_tracksGraphed.root";
  //
  //int r = system( haddFiles );	 
  //cout<<"======================================================== "<<endl;
  //cout<<"New file generated: inputs.root"<<endl;
  //cout<<"======================================================== "<<endl;

  return 0;
}
