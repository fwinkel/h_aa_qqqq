### analysisPlotter.C
This is a root macro aimed to analize the events surviving all the cuts up to JVT.
It reclusters 0.4 jets into larger R jets, and plots the dijet invariant mass.
It needs to be run where fastjet is compiled (to be improved...). For now, to compile and run do:
```
g++ analysisPlotter.C $(root-config --cflags --glibs) -o analysisPlotter `fastjet-install/bin/fastjet-config --cxxflags --libs --plugins`
./analysisPlotter

```
### sb_mjj.py
(skecth of) python script to plot S+B. Uses analysisPlotter's outfiles 

### eventDisplay.C
Nice macro to debug. Plots event display of problematic events that fulfill a given condition. To use it in your C++ code:
```c++
#include "eventDisplay.C"
...
int maxNumEventsToDisplay = 20;
vector<vector<vector<float>>> eventsToDisplay;
...
// Loop in events
for (int i=0; i<nEntries; i++){
  ...
    vector<float> pt;
    vector<float> eta;
    vector<float> phi;

    // Loop in jets
    for (int j=0; j<caloJet_pt->size(); j++){
      if(<some cut>) { continue; }        
      pt. push_back(caloJet_pt ->at(j));
      eta.push_back(caloJet_eta->at(j));
      phi.push_back(caloJet_phi->at(j));
    }
  ...
    if (eventsToDisplay.size()<maxNumEventsToDisplay){
      if (<condition>){ eventsToDisplay.push_back({pt, eta, phi}); }
  }
}
eventDisplay(eventsToDisplay);
```
### DrawATLAS.C
Root macro to plot the ATLAS style. To use it in yout c++ code:
```c++
#include "DrawATLAS.C"
...
  float xcoord = 0.14;
  float ycoord = 0.84;
  DrawATLAS(xcoord,ycoord,"AntiKt4EMPFlow",pTcut, leadpTcut);
```

