import os
import ROOT
import numpy as np

################################ PARAMETERS ###########################################################################
diCharmMass = 2*1.27 #GeV 

level = 'reco'
#level = 'truth'

treeName = 'analysis'

################################ FUNCTIONS ############################################################################

def fitter(hist, jet):
    binMax = hist.GetMaximumBin()
    peakXCoord = hist.GetXaxis().GetBinCenter(binMax)
    shift = 10000
    if   jet=='reco':  shift = 20
    elif jet=='truth': shift = 0.5
    gaussianFit = ROOT.TF1("gaussian_fit", "gaus", peakXCoord-shift,peakXCoord+shift)
    gaussianFit.SetLineColor(ROOT.kRed)
    hist.Fit(gaussianFit, "qR")
    f = hist.GetFunction('gaussian_fit')
    clone = f.Clone()
    clone.SetRange(peakXCoord-50,peakXCoord+50);
    clone.SetLineStyle(2); 
    clone.SetLineColor(ROOT.kOrange+5); 
    return f, clone

def DrawATLAS(x,y,jet,r):
    ATLAS = ROOT.TLatex()
    ATLAS.SetNDC();
    ATLAS.SetTextSize(0.03);
    ATLAS.DrawLatex(x, y - 0.03, "#sqrt{s} = 13 TeV, H #rightarrow aa #rightarrow cccc");
    ATLAS.SetTextSize(0.03);
    ATLAS.DrawLatex(x, y - 0.06, f"{jetType} Jets");
    ATLAS.SetTextSize(0.03);
    ATLAS.DrawLatex(x, y - 0.09, "m_{a} = 20 GeV");
    ATLAS.SetTextSize(0.03);
    ATLAS.DrawLatex(x, y - 0.12, r);
    ATLAS.SetTextSize(0.05);
    ATLAS.SetTextFont(72);
    ATLAS.DrawLatex(x, y, "ATLAS");
    ATLAS.SetTextSize(0.04);
    ATLAS.SetTextFont(42);
    ATLAS.DrawLatex(x, y, "                  Simulation");
    return ATLAS   

#######################################################################################################################
leading04JetPt = ROOT.TH1F('R=0.4 pT','Di-Charm jet pT',250,0,1000)
leading05JetPt = ROOT.TH1F('Reclustered R=0.5','Di-Charm jet pT',250,0,1000)
leading06JetPt = ROOT.TH1F('Reclustered R=0.6','Di-Charm jet pT',250,0,1000)
leading07JetPt = ROOT.TH1F('Reclustered R=0.7','Di-Charm jet pT',250,0,1000)
leading08JetPt = ROOT.TH1F('Reclustered R=0.8','Di-Charm jet pT',250,0,1000)

hist_nominal = ROOT.TH1D('R=0.4', 'Di-Charm jet invariant mass', 150, 0, 300)
hist_recl5   = ROOT.TH1D('R=0.5', 'Di-Charm jet invariant mass', 150, 0, 300)
hist_recl6   = ROOT.TH1D('R=0.6', 'Di-Charm jet invariant mass', 150, 0, 300)
hist_recl7   = ROOT.TH1D('R=0.7', 'Di-Charm jet invariant mass', 150, 0, 300)
hist_recl8   = ROOT.TH1D('R=0.8', 'Di-Charm jet invariant mass', 150, 0, 300)

hist_fit = ROOT.TH1D()
hist_fit.SetLineColor(ROOT.kRed)

inputs = {}
jetType = ''

recoInputs = {
               # Jet pT > 20 GeV
               "/eos/user/f/fwinkel/imGoingCrazy/gridRun/user.fwinkel.mc20_13Tev_H_a20a20_4c_recoRecl08_180723_ANALYSIS.root/user.fwinkel.34114412._000001.ANALYSIS.root": hist_recl8,  
               "/eos/user/f/fwinkel/imGoingCrazy/gridRun/user.fwinkel.mc20_13Tev_H_a20a20_4c_recoRecl07_180723_ANALYSIS.root/user.fwinkel.34114434._000001.ANALYSIS.root": hist_recl7,
               "/eos/user/f/fwinkel/imGoingCrazy/gridRun/user.fwinkel.mc20_13Tev_H_a20a20_4c_recoRecl06_180723_ANALYSIS.root/user.fwinkel.34114444._000001.ANALYSIS.root": hist_recl6,
               "/eos/user/f/fwinkel/imGoingCrazy/gridRun/user.fwinkel.mc20_13Tev_H_a20a20_4c_recoRecl05_180723_ANALYSIS.root/user.fwinkel.34114460._000001.ANALYSIS.root": hist_recl5,
               
               # Jet pT > 40 GeV
               #"/eos/user/f/fwinkel/imGoingCrazy/gridRun/user.fwinkel.mc20_13Tev_H_a20a20_4c_R08_40GeV_240723_ANALYSIS.root/user.fwinkel.34167633._000001.ANALYSIS.root": hist_recl8,
               #"/eos/user/f/fwinkel/imGoingCrazy/gridRun/user.fwinkel.mc20_13Tev_H_a20a20_4c_R07_40GeV_240723_ANALYSIS.root/user.fwinkel.34167643._000001.ANALYSIS.root": hist_recl7,
               #"/eos/user/f/fwinkel/imGoingCrazy/gridRun/user.fwinkel.mc20_13Tev_H_a20a20_4c_R06_40GeV_240723_ANALYSIS.root/user.fwinkel.34167644._000001.ANALYSIS.root": hist_recl6,
               #"/eos/user/f/fwinkel/imGoingCrazy/gridRun/user.fwinkel.mc20_13Tev_H_a20a20_4c_R05_40GeV_240723_ANALYSIS.root/user.fwinkel.34167645._000001.ANALYSIS.root": hist_recl5,

               # Jet pT > 40GeV (Z+jets)
               #"/eos/user/f/fwinkel/imGoingCrazy/backgrounds/user.fwinkel.mc20_13Tev_Zmumu_Jets_240723_ANALYSIS.root/zjets.root": hist_recl8,
             }

truthInputs = {
               #"/eos/user/f/fwinkel/imGoingCrazy/gridRun/user.fwinkel.mc20_13Tev_H_a20a20_4c_truthRecl08_170723_ANALYSIS.root/user.fwinkel.34102715._000001.ANALYSIS.root": hist_recl8, 
               #"/eos/user/f/fwinkel/imGoingCrazy/gridRun/user.fwinkel.mc20_13Tev_H_a20a20_4c_truthRecl07_170723_ANALYSIS.root/user.fwinkel.34102670._000001.ANALYSIS.root": hist_recl7,
               #"/eos/user/f/fwinkel/imGoingCrazy/gridRun/user.fwinkel.mc20_13Tev_H_a20a20_4c_truthRecl06_170723_ANALYSIS.root/user.fwinkel.34102610._000001.ANALYSIS.root": hist_recl6,
               #"/eos/user/f/fwinkel/imGoingCrazy/gridRun/user.fwinkel.mc20_13Tev_H_a20a20_4c_truthRecl05_170723_ANALYSIS.root/user.fwinkel.34102642._000001.ANALYSIS.root": hist_recl5,

               "/eos/user/f/fwinkel/imGoingCrazy/gridRun/withJetM/user.fwinkel.mc20_13Tev_H_a20a20_4c_240723_ANALYSIS.root/user.fwinkel.34161716._000001.ANALYSIS.root": hist_recl7, 
              }


pThistos = [
             leading08JetPt,          
             leading07JetPt,         
             leading06JetPt,
             leading05JetPt,
           ]



if level=='reco' : jetType = 'AntiKt4EMPFlow'; inputs = recoInputs
if level=='truth': jetType = 'AntiKt4Truth';   inputs = truthInputs

pdfName1 = 'reclusteredMjj_reco.pdf'
c = ROOT.TCanvas()
c.cd()
ROOT.gStyle.SetOptStat(0)

leg = ROOT.TLegend(.8,.8,.6,.6)

for j,fileName in enumerate(inputs):
    inputFile = ROOT.TFile(fileName, "READ")
    print("=============================")
    print("Getting tree from "+fileName)
    tree = inputFile.Get(treeName)
    # Main loop
    for entry in tree:
        reclpT1  = 99; 
        reclpT2  = 99; 
        recleta1 = 99; 
        recleta2 = 99; 
        reclphi1 = 99; 
        reclphi2 = 99;
        reclM1 = 99; 
        reclM2 = 99;  
        reclpx1 = 99  
        reclpx2 = 99
        reclpy1 = 99
        reclpy2 = 99
        reclpz1 = 99
        reclpz2 = 99

        # Read proper branches depending on jettype
        if level=='reco':
            if len(tree.reclCaloJet_pt)<2: continue
            if len(tree.CaloJet_pt)<2: continue
            reclpT1  = tree.reclCaloJet_pt[0]
            reclpT2  = tree.reclCaloJet_pt[1]
            recleta1 = tree.reclCaloJet_eta[0]
            recleta2 = tree.reclCaloJet_eta[1]
            reclphi1 = tree.reclCaloJet_phi[0] 
            reclphi2 = tree.reclCaloJet_phi[1]

            reclM1   = tree.reclCaloJet_m[0] 
            reclM2   = tree.reclCaloJet_m[1]
            reclpx1  = tree.reclCaloJet_px[0]   
            reclpx2  = tree.reclCaloJet_px[1]
            reclpy1  = tree.reclCaloJet_py[0]
            reclpy2  = tree.reclCaloJet_py[1]
            reclpz1  = tree.reclCaloJet_pz[0]
            reclpz2  = tree.reclCaloJet_pz[1]

        elif level=='truth':
            if len(tree.reclTJet_pt)<2: continue
            if len(tree.TJet_pt)<2: continue
            reclpT1  = tree.reclTJet_pt[0]
            reclpT2  = tree.reclTJet_pt[1]
            recleta1 = tree.reclTJet_eta[0]
            recleta2 = tree.reclTJet_eta[1]
            reclphi1 = tree.reclTJet_phi[0] 
            reclphi2 = tree.reclTJet_phi[1]

            reclM1   = tree.reclTJet_m[0] 
            reclM2   = tree.reclTJet_m[1]
            reclpx1  = tree.reclTJet_px[0]   
            reclpx2  = tree.reclTJet_px[1]
            reclpy1  = tree.reclTJet_py[0]
            reclpy2  = tree.reclTJet_py[1]
            reclpz1  = tree.reclTJet_pz[0]
            reclpz2  = tree.reclTJet_pz[1]

        reclp_sq1 = reclpx1**2 + reclpy1**2 + reclpz1**2
        #reclE_sq1 = reclp_sq1 + (diCharmMass)**2
        reclE_sq1 = reclp_sq1 + (reclM1)**2

        reclp_sq2 = reclpx2**2 + reclpy2**2 + reclpz2**2
        #reclE_sq2 = reclp_sq2 + (diCharmMass)**2
        reclE_sq2 = reclp_sq2 + (reclM2)**2

        # Massless case
        #m2_recl = 2*reclpT1*reclpT2*( np.cosh(recleta1-recleta2) - np.cos(reclphi1-reclphi2)  )

        # Massive case
        #m2_recl = 2*(diCharmMass)**2 + 2*(np.sqrt(reclE_sq1)*np.sqrt(reclE_sq2) - reclpx1*reclpx2 - reclpy1*reclpy2 -reclpz1*reclpz2)
        m2_recl = reclM1**2 + reclM2**2 + 2*(np.sqrt(reclE_sq1)*np.sqrt(reclE_sq2) - reclpx1*reclpx2 - reclpy1*reclpy2 -reclpz1*reclpz2)

        inputs[fileName].Fill(np.sqrt(m2_recl))
        pThistos[j].Fill(reclpT1)
        if (j==len(inputs)-1):
            pT1  = 99; 
            pT2  = 99; 
            eta1 = 99; 
            eta2 = 99; 
            phi1 = 99; 
            phi2 = 99;

            M1 = 99; 
            M2 = 99;
            px1 = 99  
            px2 = 99
            py1 = 99
            py2 = 99
            pz1 = 99
            pz2 = 99
            if level=='truth':
                pT1  = tree.TJet_pt[0]
                pT2  = tree.TJet_pt[1]
                eta1 = tree.TJet_eta[0]
                eta2 = tree.TJet_eta[1]
                phi1 = tree.TJet_phi[0]
                phi2 = tree.TJet_phi[1]
                M1   = tree.TJet_m[0]
                M2   = tree.TJet_m[1]

                px1  = tree.TJet_px[0]   
                px2  = tree.TJet_px[1]
                py1  = tree.TJet_py[0]
                py2  = tree.TJet_py[1]
                pz1  = tree.TJet_pz[0]
                pz2  = tree.TJet_pz[1]

            elif level=='reco':
                pT1  = tree.CaloJet_pt[0]
                pT2  = tree.CaloJet_pt[1]
                eta1 = tree.CaloJet_eta[0]
                eta2 = tree.CaloJet_eta[1]
                phi1 = tree.CaloJet_phi[0]
                phi2 = tree.CaloJet_phi[1]
                M1 = tree.CaloJet_m[0]
                M2 = tree.CaloJet_m[1]

                px1  = tree.CaloJet_px[0]   
                px2  = tree.CaloJet_px[1]
                py1  = tree.CaloJet_py[0]
                py2  = tree.CaloJet_py[1]
                pz1  = tree.CaloJet_pz[0]
                pz2  = tree.CaloJet_pz[1]

            p_sq1 = px1**2 + py1**2 + pz1**2
            #E_sq1 = p_sq1 + (diCharmMass)**2
            E_sq1 = p_sq1 + (M1)**2

            p_sq2 = px2**2 + py2**2 + pz2**2
            #E_sq2 = p_sq2 + (diCharmMass)**2
            E_sq2 = p_sq2 + (M2)**2
   
            # Massless case          
            #m2 = 2*pT1*pT2*(np.cosh(eta1-eta2) - np.cos(phi1-phi2))

            # Massive case
            #m2 = (np.sqrt(E_sq1) + np.sqrt(E_sq2))**2 - ( (px1+px2)**2 + (py1+py2)**2 + (pz1+pz2)**2 )
            #m2 = 2*(diCharmMass)**2 + 2*(np.sqrt(E_sq1)*np.sqrt(E_sq2) - px1*px2 - py1*py2 -pz1*pz2)
            m2 = M1**2 + M2**2 + 2*(np.sqrt(E_sq1)*np.sqrt(E_sq2) - px1*px2 - py1*py2 -pz1*pz2)
            
            hist_nominal.Fill(np.sqrt(m2))
            leading04JetPt.Fill(pT1)
    inputs[fileName].Scale(1/inputs[fileName].Integral('width')) 
    inputs[fileName].SetLineColor(j+3)
    inputs[fileName].GetXaxis().SetTitle('m_{jj} [GeV]')
    inputs[fileName].Draw('HIST SAME')
    leg.AddEntry(inputs[fileName],inputs[fileName].GetName())
    if (j==len(inputs)-1): 
        hist_nominal.Scale(1/hist_nominal.Integral('width')); 
        hist_nominal.SetLineColor(1)
        hist_nominal.Draw('HIST SAME'); 
        leg.AddEntry(hist_nominal,hist_nominal.GetName())
    leg.Draw('SAME')
    ROOT.gPad.SetTicks(1);
    ROOT.gPad.Update();

    xcoord = 0.14;
    ycoord = 0.84;
    DrawATLAS(xcoord,ycoord,jetType,'')

c.Print(pdfName1)

#################### FITTER #############################################################################

pdfName2 = 'sigmaComparison.pdf'

c1 = ROOT.TCanvas()
c1.cd()

c1.Print(pdfName2+'[')

for j,fileName in enumerate(inputs):
    leg1 = ROOT.TLegend(.9,.9,.5,.5)
    inputs[fileName].Scale(1/inputs[fileName].Integral('width')) 
    inputs[fileName].SetLineColor(ROOT.kGreen)
    inputs[fileName].GetXaxis().SetTitle('m_{jj} [GeV]')
    if jetType=='truth': inputs[fileName].GetYaxis().SetRangeUser(0,0.18)
    inputs[fileName].Draw('HIST')
    fit, extension = fitter(inputs[fileName], level)
    extension.SetLineColor(ROOT.kBlue+3)   
    fit.SetLineColor(ROOT.kBlue)   
    extension.Draw('SAME')
    fit.Draw('SAME')
    leg1.AddEntry(inputs[fileName],inputs[fileName].GetName()+",  #frac{#sigma_{fit}}{#mu_{fit}} = %.3f"%(fit.GetParameter(2)/fit.GetParameter(1)))
   
    hist_nominal.Scale(1/hist_nominal.Integral('width')); 
    hist_nominal.SetLineColor(1)
    if jetType=='truth': hist_nominal.GetYaxis().SetRangeUser(0,0.18)
    hist_nominal.Draw('HIST SAME'); 
    fit1, extension1 = fitter(hist_nominal, level)
    extension1.Draw('SAME')
    fit1.Draw('SAME')
    leg1.AddEntry( hist_nominal,hist_nominal.GetName()+",  #frac{#sigma_{fit}}{#mu_{fit}} = %.3f"%(fit1.GetParameter(2)/fit1.GetParameter(1)) )
    leg1.AddEntry(fit1, "Gaussian fit"+", #sigma_{fit} = %.1f, #mu_{fit} = %.0f"%(fit1.GetParameter(2), fit1.GetParameter(1)))
    leg1.AddEntry(fit,  "Gaussian fit"+", #sigma_{fit} = %.1f, #mu_{fit} = %.0f"%(fit.GetParameter(2), fit.GetParameter(1)))
    leg1.Draw('SAME')
    ROOT.gPad.SetTicks(1);
    ROOT.gPad.Update();

    xcoord = 0.14;
    ycoord = 0.84;
    DrawATLAS(xcoord,ycoord,jetType,'')

    c1.Print(pdfName2)

c1.Print(pdfName2+']')


#################### LEADING JET PT #############################################################################

pdfName3 = 'jetPt.pdf'
c2 = ROOT.TCanvas()
c2.cd()

c2.Print(pdfName3+'[')
ROOT.gPad.SetLogy()
xcoord = 0.54;
ycoord = 0.84;
for h in pThistos:
    ROOT.gPad.SetTicks(1);
    ROOT.gPad.Update();
    h.GetXaxis().SetTitle('Leading jet p_{T} [GeV]')   
    h.Draw('HIST')
    DrawATLAS(xcoord,ycoord,jetType,h.GetName())
    c2.Print(pdfName3)

leading04JetPt.GetXaxis().SetTitle('Leading jet p_{T} [GeV]')
leading04JetPt.Draw('HIST')
DrawATLAS(xcoord,ycoord,jetType,'Not reclustered')
ROOT.gPad.SetTicks(1);
ROOT.gPad.Update();
c2.Print(pdfName3)

c2.Print(pdfName3+']')


#################### OUTPUT #################################################################################

# Merge output in a single pdf
outPdf = 'mjj_reclusteringStudies.pdf'

command = f'pdfunite {pdfName1} {pdfName2} {pdfName3} {outPdf}' 
os.system(command)

print("======================================")
print("======= Done plotting ================")
print("======================================")
print("")
print(f"For a fast copying: {os.getcwd()}/{outPdf}")
print("")
