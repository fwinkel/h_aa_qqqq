import ROOT
import numpy as np

################# Parameters #######################################
#L        = 
#xsec_s   = 
#xsec_b   = 
#nSignals = 
#nBackgrs =
jetType = "AntiKt4"
pTcut     = 10 
leadpTcut = 20
############################ #######################################

signalFile = ROOT.TFile("signal_invariantMassHistos.root","READ")
backgrFile = ROOT.TFile("backgr_invariantMassHistos.root","READ")

signalHist = signalFile.Get("R=0.4")
backgrHist = backgrFile.Get("R=0.4")

#sf_signal = L*xsec_s/nSignals
#sf_backgr = L*xsec_b/nBackgrs
#
#signalHist.Scale(sf_signal) 
#backgrHist.Scale(sf_backgr)

c = ROOT.TCanvas()
c.cd()

signalHist.Add(backgrHist)
signalHist.GetXaxis().SetTitle("m_{jj}[GeV]")
signalHist.Draw("HIST SAME")

x = 0.5;
y = 0.84;

ATLAS = ROOT.TLatex();
ATLAS.SetNDC();
ATLAS.SetTextSize(0.03);
ATLAS.DrawLatex(x, y - 0.03, "#sqrt{s} = 13 TeV, H #rightarrow aa #rightarrow cccc");
ATLAS.SetTextSize(0.03);
ATLAS.DrawLatex(x, y - 0.06, f"{jetType} Jets");
ATLAS.SetTextSize(0.03);
ATLAS.DrawLatex(x, y - 0.09, "m_{a} = 20 GeV");
ATLAS.SetTextSize(0.03);
ATLAS.DrawLatex(x, y - 0.12, f"Jet pT>{pTcut}");
ATLAS.SetTextSize(0.03);
ATLAS.DrawLatex(x, y - 0.15, f"Leading Jets pT>{leadpTcut}");
ATLAS.SetTextSize(0.05);
ATLAS.SetTextFont(72);
ATLAS.DrawLatex(x, y, "ATLAS");
ATLAS.SetTextSize(0.04);
ATLAS.SetTextFont(42);
ATLAS.DrawLatex(x, y, "                  Simulation");

input("")
c.Print("signalBackground.pdf")
