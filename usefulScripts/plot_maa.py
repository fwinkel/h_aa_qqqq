import os
import ROOT
import numpy as np

treeName = 'analysis'

hist_ma_4  = ROOT.TH1D('m_{a}=4GeV',  'BSM scalar invariant mass', 140, 80, 150)
hist_ma_8  = ROOT.TH1D('m_{a}=8GeV',  'BSM scalar invariant mass', 140, 80, 150)
hist_ma_12 = ROOT.TH1D('m_{a}=12GeV', 'BSM scalar invariant mass', 140, 80, 150)
hist_ma_16 = ROOT.TH1D('m_{a}=16GeV', 'BSM scalar invariant mass', 140, 80, 150)
hist_ma_20 = ROOT.TH1D('m_{a}=20GeV', 'BSM scalar invariant mass', 140, 80, 150)

hist_fit = ROOT.TH1D()
hist_fit.SetLineColor(ROOT.kRed)

inputs = {
          "./user.fwinkel.mc20_13Tev_H_a20a20_4c_200723_ANALYSIS.root/user.fwinkel.34134771._000001.ANALYSIS.root": hist_ma_20,
          "./user.fwinkel.mc20_13Tev_H_a16a16_4c_200723_ANALYSIS.root/user.fwinkel.34134764._000001.ANALYSIS.root": hist_ma_16,      
          "./user.fwinkel.mc20_13Tev_H_a12a12_4c_200723_ANALYSIS.root/user.fwinkel.34134755._000001.ANALYSIS.root": hist_ma_12,
          "./user.fwinkel.mc20_13Tev_H_a8a8_4c_200723_ANALYSIS.root/user.fwinkel.34134754._000001.ANALYSIS.root  ": hist_ma_8,
          "./user.fwinkel.mc20_13Tev_H_a4a4_4c_200723_ANALYSIS.root/user.fwinkel.34134752._000001.ANALYSIS.root  ": hist_ma_4,
         }

aMass    = [20,16,12,8,4]
pdfName1 = 'maa.pdf'
colors = [ROOT.kRed,ROOT.kBlack,ROOT.kGreen,ROOT.kAzure,ROOT.kMagenta]
c = ROOT.TCanvas()
c.cd()
ROOT.gStyle.SetOptStat(0)

#leg = ROOT.TLegend(1,1,1,1)
leg = ROOT.TLegend(.4,.4,.2,.2)

for j,fileName in enumerate(inputs):
    inputFile = ROOT.TFile(fileName, "READ")
    print("=============================")
    print("Getting tree from "+fileName)
    tree = inputFile.Get(treeName)

    for entry in tree:
        if len(tree.BSMParticle_pt)<2: continue
        if len(tree.BSMParticle_pt)<2: continue
        pT1  = tree.BSMParticle_pt[2]  # BSMTruth container has 4 truth BSM bosons per event; 
        pT2  = tree.BSMParticle_pt[3]  # 0 and 1 are H0, 2 and 3 are a's    
        eta1 = tree.BSMParticle_eta[2]
        eta2 = tree.BSMParticle_eta[3]
        phi1 = tree.BSMParticle_phi[2] 
        phi2 = tree.BSMParticle_phi[3]
        #m2 = 2*pT1*pT2*( np.cosh(eta1-eta2) - np.cos(phi1-phi2)  )

        px1   = pT1*np.cos( phi1)
        py1   = pT1*np.sin( phi1)
        pz1   = pT1*np.sinh(eta1)
        p_sq1 = px1**2 + py1**2 + pz1**2
        E_sq1 = p_sq1 + (aMass[j])**2

        px2   = pT2*np.cos( phi2)
        py2   = pT2*np.sin( phi2)
        pz2   = pT2*np.sinh(eta2)
        p_sq2 = px2**2 + py2**2 + pz2**2
        E_sq2 = p_sq2 + (aMass[j])**2

        # Massive case
        #m2 = (np.sqrt(E_sq1) + np.sqrt(E_sq2))**2 - ( (px1+px2)**2 + (py1+py2)**2 + (pz1+pz2)**2 )
        m2 = 2*aMass[j]**2 + 2*(np.sqrt(E_sq1)*np.sqrt(E_sq2) - px1*px2 - py1*py2 -pz1*pz2)

        inputs[fileName].Fill(np.sqrt(m2))
    inputs[fileName].Scale(1/inputs[fileName].Integral('width')) 
    inputs[fileName].SetLineColor(colors[j])
    inputs[fileName].GetXaxis().SetTitle('m_{aa} [GeV]')
    inputs[fileName].GetYaxis().SetRangeUser(0,1.7)
    inputs[fileName].Draw('HIST SAME')
    leg.AddEntry(inputs[fileName],inputs[fileName].GetName())
    leg.Draw('SAME')
    ROOT.gPad.SetTicks(1);
    ROOT.gPad.Update();

    higgs = ROOT.TText(126,1,'Higgs (125.3GeV)')
    higgs.SetTextColor(ROOT.kRed+2)    
    higgs.Draw('same')

    v_line = ROOT.TLine(125.3,0,125.3,1.7); 
    v_line.SetLineColor(ROOT.kRed+2);
    v_line.SetLineWidth(2);
    v_line.SetLineStyle(ROOT.kDashed);
    v_line.Draw("same");

    xcoord = 0.14;
    ycoord = 0.84;

    ATLAS = ROOT.TLatex()
    ATLAS.SetNDC();
    ATLAS.SetTextSize(0.03);
    ATLAS.DrawLatex(xcoord, ycoord - 0.03, "#sqrt{s} = 13 TeV, H #rightarrow aa #rightarrow cccc");
    ATLAS.SetTextSize(0.03);
    ATLAS.DrawLatex(xcoord, ycoord - 0.06, "Truth particles");
    ATLAS.SetTextSize(0.05);
    ATLAS.SetTextFont(72);
    ATLAS.DrawLatex(xcoord, ycoord, "ATLAS");
    ATLAS.SetTextSize(0.04);
    ATLAS.SetTextFont(42);
    ATLAS.DrawLatex(xcoord, ycoord, "                  Simulation");

c.Print(pdfName1)

print("======================================")
print("======= Done plotting ================")
print("======================================")
print("")
print("")
