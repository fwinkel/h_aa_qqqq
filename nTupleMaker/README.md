# nTupleMaker

This is the section of the framework that generates ntuples from DAODs. 
Below are the instructions for setup, a local run and a grid run.

#### DISCLAIMER: For every instruction given below is assumed that you are working on lxplus.

## Setup
To use the nTupleMaker you must:

 1. Clone the whole repository in an empty directory ```X```
 2. It will be named ```h_aa_qqqq```. Change it's name to ```source``` 
 3. ```mkdir build run```
 4. Create a file named "Setup.sh" with the following content:
   ```bash
   setupATLAS
   cd build
   asetup AnalysisBase 24.2.27
   cmake ../source/
   make
   source x86_64-*/setup.sh
   cd ..
   ```
 5. To compile, do ```source Setup.sh```   

## Local run

 Go to the run directory and do:
```bash
ATestRun_eljob.py --submission-dir=submitDir 
```

## Grid run

  1. Move to the once empty directory ```X```
  2. ```mkdir gridRun```
  3. ```cd gridRun```
  4. ```cp ../source/nTupleMaker/share/runGrid.py .```
  5. Specify the samples you want to run over in the samples dictionary, choose adequate names 
  6. If already compiled, ```lsetup rucio panda```  
  7. ```python3 runGrid.py```

## Event selection

### Trigger
  Events must pass either HLT_mu50 or HLT_mu26_ivarmedium
### Vertices
  At least 1 PV
### Muons
  * At least two muons
  * At least one muon and one antimuon
  * Both mu/antimu must pass TTVA recommendations:
    - |d0_sig|<3 
    - |delta_z0*sin(theta)|<0.5mm
### Jets
  * At least two jets
  * Pass JVT
  * At least two jets with 20GeV and |\eta|<2.5
  * At least two leading jets with 20GeV

