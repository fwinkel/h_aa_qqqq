// #ifndef JET_UTILS_H
// #define JET_UTILS_H

// #include <xAODTracking/TrackParticleContainerFwd.h>
// #include <xAODJet/JetFwd.h>

// #include <AnaAlgorithm/AnaAlgorithm.h>
// #include <AsgTools/AnaToolHandle.h>
// #include <EventLoop/Algorithm.h>


// #include <ParticleJetTools/JetParticleShrinkingConeAssociation.h>
// #include <FlavorTagDiscriminants/BTagTrackIpAccessor.h>

// class JetTool
// {
// public:
//   TrackTool(const std::string& jet_track_link = "JetTracks");

//   void decorate_jets_with_tracks(const xAOD::JetContainer* jets, const xAOD::TrackParticleContainer* tracks);
//   // typedef std::vector<const xAOD::TrackParticle*> Tracks;
//   // vector< vector< ElementLink<xAOD::IParticleContainer> > > associate(const xAOD::Jet& jet) const;
//   // Tracks get_tracks(const xAOD::Jet& jet) const;

// private:
//   typedef SG::AuxElement AE;
//   typedef std::vector<ElementLink<xAOD::TrackParticleContainer> > TrackLinks;
//   typedef std::vector<ElementLink<xAOD::IParticleContainer> > PartLinks;

//   asg::AnaToolHandle<JetParticleShrinkingConeAssociation>  m_JetParticleShrinkingConeAssociation_handle {"JetParticleShrinkingConeAssociation"};
//   std::string m_jet_track_link;

//   bool passed_cuts(const xAOD::TrackParticle &tp) const;
//   // TrackSelectorConfig::Cuts m_track_select_cfg;

//   BTagTrackIpAccessor m_acc;
// };

// #endif
