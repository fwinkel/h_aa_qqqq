#ifndef TRACK_TOOL_H
#define TRACK_TOOL_H

#include <AsgTools/AnaToolHandle.h>
#include <ParticleJetTools/JetParticleShrinkingConeAssociation.h>
#include <FlavorTagDiscriminants/BTagTrackIpAccessor.h>

class TrackTool
{
public:
  std::string m_jet_track_link;
  TrackTool(const std::string& jet_track_link = "JetTracks");

  void decorateJetsWithTracks(const xAOD::JetContainer* jets, const xAOD::TrackParticleContainer* tracks);

  ~TrackTool () {
     // delete m_jetEta;
     // delete m_jetPhi;
     // delete m_jetPt;
     // delete m_jetE;
     // delete m_muonEta;
     // delete m_muonPhi;
     // delete m_muonPt;
     // delete m_muonE;
   }


private:
  asg::AnaToolHandle<JetParticleShrinkingConeAssociation>  m_JetParticleShrinkingConeAssociation_handle {"JetParticleShrinkingConeAssociation"};

  bool passTrackSelection(const xAOD::TrackParticle& tp) const;

  BTagTrackIpAccessor m_acc;
};

#endif
