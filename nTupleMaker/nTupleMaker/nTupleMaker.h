#ifndef MyAnalysis_nTupleMaker_H
#define MyAnalysis_nTupleMaker_H

#include <iostream>
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TH1.h>
#include <TTree.h>
#include <vector>
#include <AsgTools/AnaToolHandle.h>

#include <TrigConfInterfaces/ITrigConfigTool.h>
#include <TrigDecisionTool/TrigDecisionTool.h>
#include <MuonEfficiencyCorrections/MuonTriggerScaleFactors.h>

//#include <PileupReweighting/PileupReweightingTool.h>
#include "AsgAnalysisInterfaces/IPileupReweightingTool.h"


#include <MuonSelectorTools/MuonSelectionTool.h>
#include <MuonMomentumCorrections/MuonCalibTool.h>
#include <MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h>
#include <IsolationSelection/IsolationSelectionTool.h>
#include <TrackVertexAssociationTool/TrackVertexAssociationTool.h>

#include <JetSelectorTools/IEventCleaningTool.h>
#include <JetAnalysisInterfaces/IJetJvtEfficiency.h>
#include <JetMomentTools/JetVertexTaggerTool.h>
#include <JetCalibTools/IJetCalibrationTool.h>

#include <ParticleJetTools/JetParticleShrinkingConeAssociation.h>

#include <EventLoop/IWorker.h>
#include <EventLoop/Algorithm.h>

#include <nTupleMaker/TrackTool.h>

#include <FlavorTagDiscriminants/GNN.h>

// ONNX Library
#include <core/session/onnxruntime_cxx_api.h>

#include <PathResolver/PathResolver.h>

class nTupleMaker : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  nTupleMaker (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode fileExecute () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  ~nTupleMaker () {
    // delete m_jetEta;
    // delete m_jetPhi;
    // delete m_jetPt;
    // delete m_jetE;
    // delete m_muonEta;
    // delete m_muonPhi;
    // delete m_muonPt;
    // delete m_muonE;
  }

private:
  // Configuration, and any other types of variables go here.
  
  // Script options / configurations
  // These get `m_prop_` prefixes and are declared as AnaAlgorithm properties in `nTupleMaker.cxx`
  std::string    m_prop_sampleName;
  bool           m_prop_finalOnly;          // only store events that pass all cuts
  bool           m_prop_storeTracks;        // store tracks
  int            m_prop_systematics;        // which systematics to run; 0 means nominal
  double         m_prop_muonPtCut;
  double         m_prop_jetPtCut;
  // double         m_prop_leadingJetPtCut;
  // double         m_prop_reclusterDeltaR;
  std::string    m_prop_taggerOnnxPath;
  std::string    m_derivationName = "";

  // Variables for control flow
  bool m_isFirstEvent;
  bool m_isMC = false;

  //Cutflow hist
  TH1F* m_cutflowHist    = nullptr;  //!
  // TH1F* m_cutflowHistW   = nullptr;  //!
  TH1D* m_histSumW = nullptr;

  // Event cuts
  static constexpr int n_cuts = 9;
  const char *eventCuts[n_cuts] = {
    "All",
    "EventClean",
    "Pass Trigger", 
    "Primary Vertex",
    "#geq 2 Muons",
    "Muon ID/Iso/TTVA",
    "m_{ll} = m_{Z}",
    "#geq 2 Jets",
    "Final"
  };
  // int m_cutflow_all;
  // int m_cutflow_eventClean;
  // int m_cutflow_trigger;
  // int m_cutflow_vertex;
  // int m_cutflow_muons;
  // int m_cutflow_muonsQual;
  // int m_cutflow_muonsMZ;
  // int m_cutflow_jets;
  // int m_cutflow_final;

  // Sum of weights
  float sumOfWeights;
  float m_MD_initialNevents;
  float m_MD_initialSumW;
  float m_MD_initialSumWSquared;
  float m_MD_finalNevents;  
  float m_MD_finalSumW;  
  float m_MD_finalSumWSquared;  


  // Event counter
  int count = 0;

  // Event-level variables
  unsigned int       m_runNumber                                      = 0; ///< Run number
  unsigned int       m_channelNumber                                  = 0; ///< MC channel number
  unsigned long long m_eventNumber                                    = 0; ///< Event number
  float              m_averageInteractionsPerCrossing                 = 0; 
  float              m_actualInteractionsPerCrossing                  = 0;
  float              m_eventWeight                                    = 1;
  float              m_pileupWeight                                   = 1;
  float              m_pileupWeightUp                                 = 1;
  float              m_pileupWeightDown                               = 1;
  double             m_triggerSF                                      = 1;
  int                m_correctedScaled_averageInteractionsPerCrossing = 0;
  int                m_correctedScaled_actualInteractionsPerCrossing  = 0;
  int                m_corrected_actualInteractionsPerCrossing        = 0;
  int                m_triggerPassed                                  = 999;

  int m_event_cutLevel;

  // Reco Muons
  std::vector<float> *m_muonPos_pt      = nullptr;
  std::vector<float> *m_muonPos_eta     = nullptr;
  std::vector<float> *m_muonPos_phi     = nullptr;
  std::vector<float> *m_muonPos_e       = nullptr;
  std::vector<float> *m_muonPos_SF      = nullptr;
  std::vector<float> *m_muonNeg_pt      = nullptr;
  std::vector<float> *m_muonNeg_eta     = nullptr;
  std::vector<float> *m_muonNeg_phi     = nullptr;
  std::vector<float> *m_muonNeg_e       = nullptr;
  std::vector<float> *m_muonNeg_SF      = nullptr;

  // Reco Jets
  std::vector<float> *m_jet_pt       = nullptr;
  std::vector<float> *m_jet_eta      = nullptr;
  std::vector<float> *m_jet_phi      = nullptr;
  std::vector<float> *m_jet_e        = nullptr;
  std::vector<float> *m_jet_m        = nullptr;
  std::vector<float> *m_jet_px       = nullptr;
  std::vector<float> *m_jet_py       = nullptr;
  std::vector<float> *m_jet_pz       = nullptr;
  std::vector<int>   *m_jet_truthId  = nullptr;
  std::vector<int>   *m_jet_nTracks  = nullptr;
  // Jet tagger scores
  std::vector<float> *m_jet_pcc      = nullptr;
  std::vector<float> *m_jet_pc       = nullptr;
  std::vector<float> *m_jet_pb       = nullptr;
  std::vector<float> *m_jet_pu       = nullptr;
  // Jet-Tracks (tracks associated to a reco. jet)
  std::vector<std::vector<double>> *m_jetTrack_pt                          = nullptr;
  std::vector<std::vector<double>> *m_jetTrack_eta                         = nullptr;
  std::vector<std::vector<double>> *m_jetTrack_phi                         = nullptr;
  std::vector<std::vector<double>> *m_jetTrack_e                           = nullptr;
  std::vector<std::vector<double>> *m_jetTrack_theta                       = nullptr;
  std::vector<std::vector<double>> *m_jetTrack_d0                          = nullptr;     
  std::vector<std::vector<double>> *m_jetTrack_z0SinTheta                  = nullptr;      
  std::vector<std::vector<double>> *m_jetTrack_dphi                        = nullptr;      
  std::vector<std::vector<double>> *m_jetTrack_deta                        = nullptr;      
  std::vector<std::vector<double>> *m_jetTrack_qOverP                      = nullptr;
  std::vector<std::vector<double>> *m_jetTrack_IP3D_signed_d0_sig          = nullptr;       
  std::vector<std::vector<double>> *m_jetTrack_IP3D_signed_z0_sig          = nullptr;      
  std::vector<std::vector<double>> *m_jetTrack_phiUncert                   = nullptr; 
  std::vector<std::vector<double>> *m_jetTrack_thetaUncert                 = nullptr; 
  std::vector<std::vector<double>> *m_jetTrack_qOverPUncert                = nullptr;   
  std::vector<std::vector<int>>    *m_jetTrack_numPixHits                  = nullptr;       
  std::vector<std::vector<int>>    *m_jetTrack_numSCTHits                  = nullptr;           
  std::vector<std::vector<int>>    *m_jetTrack_numIPLHits                  = nullptr;           
  std::vector<std::vector<int>>    *m_jetTrack_numNIPLHits                 = nullptr;           
  std::vector<std::vector<int>>    *m_jetTrack_numIPLSharedHits            = nullptr;       
  std::vector<std::vector<int>>    *m_jetTrack_numIPLSplitHits             = nullptr;       
  std::vector<std::vector<int>>    *m_jetTrack_numPixSharedHits            = nullptr;            
  std::vector<std::vector<int>>    *m_jetTrack_numPixSplitHits             = nullptr;            
  std::vector<std::vector<int>>    *m_jetTrack_numSCTSharedHits            = nullptr;       
  std::vector<std::vector<int>>    *m_jetTrack_numPixHoles                 = nullptr;                 
  std::vector<std::vector<int>>    *m_jetTrack_numSCTHoles                 = nullptr;       
  std::vector<std::vector<int>>    *m_jetTrack_leptonID                    = nullptr;       

  // Truth jets
  std::vector<float> *m_truthJet_pt         = nullptr;
  std::vector<float> *m_truthJet_eta        = nullptr;
  std::vector<float> *m_truthJet_phi        = nullptr;
  std::vector<float> *m_truthJet_e          = nullptr;
  std::vector<float> *m_truthJet_m          = nullptr;
  std::vector<float> *m_truthJet_px         = nullptr;
  std::vector<float> *m_truthJet_py         = nullptr;
  std::vector<float> *m_truthJet_pz         = nullptr;
  std::vector<int>   *m_truthJet_pdgId      = nullptr;

  // Reclustered reco jets
  std::vector<float> *m_reclJet_pt          = nullptr;
  std::vector<float> *m_reclJet_eta         = nullptr;
  std::vector<float> *m_reclJet_phi         = nullptr;
  std::vector<float> *m_reclJet_e           = nullptr;
  std::vector<float> *m_reclJet_m           = nullptr;
  std::vector<float> *m_reclJet_r           = nullptr;
  std::vector<float> *m_reclJet_px          = nullptr;
  std::vector<float> *m_reclJet_py          = nullptr;
  std::vector<float> *m_reclJet_pz          = nullptr;
  std::vector<int>   *m_nReclJets           = nullptr; // < Number of reclustered jets

  // Reclustered truth jets
  std::vector<float> *m_reclTruthJet_eta    = nullptr;
  std::vector<float> *m_reclTruthJet_phi    = nullptr;
  std::vector<float> *m_reclTruthJet_pt     = nullptr;
  std::vector<float> *m_reclTruthJet_e      = nullptr;
  std::vector<float> *m_reclTruthJet_m      = nullptr;
  std::vector<float> *m_reclTruthJet_r      = nullptr;
  std::vector<float> *m_reclTruthJet_px     = nullptr;
  std::vector<float> *m_reclTruthJet_py     = nullptr;
  std::vector<float> *m_reclTruthJet_pz     = nullptr;
  std::vector<int>   *m_reclTruthJet_pdgId  = nullptr;

  // BSM scalar
  std::vector<float> *m_scalar_pt            = nullptr;
  std::vector<float> *m_scalar_eta           = nullptr;
  std::vector<float> *m_scalar_phi           = nullptr;
  std::vector<float> *m_scalar_e             = nullptr;
  std::vector<float> *m_scalar_m             = nullptr;
  std::vector<int>   *m_scalar_pdgId         = nullptr;

  // Truth charms
  std::vector<float> *m_charm_pt                     = nullptr;
  std::vector<float> *m_charm_eta                    = nullptr;
  std::vector<float> *m_charm_phi                    = nullptr;
  std::vector<float> *m_charm_e                      = nullptr;
  std::vector<float> *m_charm_m                      = nullptr;
  std::vector<int>   *m_charm_pdgId                  = nullptr;
  std::vector<std::vector<float>> *m_charm_childPt   = nullptr;
  std::vector<std::vector<float>> *m_charm_childEta  = nullptr;
  std::vector<std::vector<float>> *m_charm_childPhi  = nullptr;
  std::vector<std::vector<int>>   *m_charm_parentId  = nullptr;

  // Truth tools
  std::vector<std::function<bool(const xAOD::Jet&)>> m_overlap_checks;

  // Athena tools
  asg::AnaToolHandle<TrigConf::ITrigConfigTool>    m_trigConfTool_handle      {"TrigConf::xAODConfigTool/xAODConfigTool", this}; //!
  asg::AnaToolHandle<Trig::TrigDecisionTool>       m_trigDecTool_handle       {"Trig::TrigDecisionTool/TrigDecisionTool"}; //!
  // TODO: Wrap these in AnaToolHandle where possible
  // TODO: Wrap these into a separate tool class like `TrackTool` where appropriate
  //asg::AnaToolHandle<CP::IPileupReweightingTool>   m_prwTool_handle           {"CP::PileupReweightingTool/tool"};
  asg::AnaToolHandle<CP::IPileupReweightingTool>   m_prwTool_handle           {"CP::PileupReweightingTool/Pileup"}; //!
  CP::MuonCalibTool* m_muonCalibTool;
  CP::MuonSelectionTool *m_muonIDSelectionTool;
  CP::IsolationSelectionTool *m_muonIsoSelectionTool;
  CP::TrackVertexAssociationTool *m_muonTTVATool;
  // TODO: Add these back in and validate once PRW is implemented
  CP::MuonEfficiencyScaleFactors *m_muonEffToolID;
  CP::MuonEfficiencyScaleFactors *m_muonEffToolIso;
  CP::MuonEfficiencyScaleFactors *m_muonEffToolTTVA;
  CP::MuonTriggerScaleFactors    *m_muonTrigSFTool;
  asg::AnaToolHandle<IJetCalibrationTool>       m_JetCalibrationTool_handle   {"JetCalibrationTool" , this};
  asg::AnaToolHandle<JetVertexTaggerTool>       m_JVT_tool_handle             {"JetVertexTaggerTool"};//!
  FlavorTagDiscriminants::GNN *m_GNNTagger;
  // Custom tools
  TrackTool *m_TrackTool;

  StatusCode autoconfigurePileupRWTool();

protected:

  // This if for getting some track-related objects via element link
  template<typename T, typename U, typename V> void safeFill(const V* xAODObj, SG::AuxElement::ConstAccessor<T>& accessor, std::vector<U>* destination, U defaultValue, int units = 1){
    if ( accessor.isAvailable( *xAODObj ) ) {
      destination->push_back( accessor( *xAODObj ) / units );
    } else {
      destination->push_back( defaultValue );
    }
  }

};

#endif
