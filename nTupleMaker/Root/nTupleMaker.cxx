#include <typeinfo>
#include <limits>
#include <filesystem>
// #include <boost/filesystem.hpp>

#include <AsgMessaging/MessageCheck.h>
#include <PathResolver/PathResolver.h>
#include "nTupleMaker/nTupleMaker.h"
#include "nTupleMaker/TrackTool.h"

#include <xAODEventInfo/EventInfo.h>
#include <xAODCaloEvent/CaloClusterContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTracking/VertexContainer.h>
#include <xAODTracking/TrackParticlexAODHelpers.h>
#include <xAODCutFlow/CutBookkeeper.h>
#include <xAODCutFlow/CutBookkeeperContainer.h>
#include <xAODTruth/TruthParticle.h>
#include <xAODCore/ShallowCopy.h>

//#include "TrigConfxAOD/xAODConfigTool.h"

#include <JetJvtEfficiency/JetJvtEfficiency.h>

#include <fastjet/ClusterSequence.hh>

#include <EventLoop/Worker.h>
#include <EventLoop/Algorithm.h>

#include <FlavorTagDiscriminants/BTagTrackIpAccessor.h>
#include <FlavorTagDiscriminants/customGetter.h>
#include <FlavorTagDiscriminants/FlipTagEnums.h>

//#include "PileupReweighting/PileupReweightingTool.h"


using namespace std;

nTupleMaker :: nTupleMaker (const string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  m_isFirstEvent = true;

  declareProperty( "isMC", m_isMC = true,
                   "Boolean to determine if running over data or MC" );

  declareProperty( "sampleName", m_prop_sampleName = "Unknown",
                   "Descriptive name for the processed sample" );

  declareProperty( "derivationName", m_derivationName = "Unknown",
                   "Descriptive name for the processed sample" );

  declareProperty( "finalOnly", m_prop_finalOnly = false,
                   "If true, only events passing all cuts are stored into output ntuple" );

  declareProperty( "storeTracks", m_prop_storeTracks = false,
                   "If true, all jet-tracks are stored into output ntuple" );

  declareProperty( "systematics", m_prop_systematics = 0,
                   "Systematics to vary. 0 = nominal settings" );

  declareProperty( "muonPtCut", m_prop_muonPtCut = 10000.0,
                   "Minimum muon pT (in MeV)" );

  declareProperty( "jetPtCut", m_prop_jetPtCut = 20000.0,
                   "Minimum jet pT (in MeV)" );

  // declareProperty( "leadingJetPtCut", m_prop_leadingJetPtCut = 20000.0, 
  //                  "Minimum leading jet pT (in MeV)" );

  // declareProperty( "reclusterDeltaR", m_prop_reclusterDeltaR = 0.6,
  //                  "Size of reclustered jets" );

  declareProperty( "taggerOnnxPath", m_prop_taggerOnnxPath = "nTupleMaker/tagger/GN2-S3_20231028-T164149/network.onnx",
                   "Path to jet tagger ONNX file" );
}


StatusCode nTupleMaker :: fileExecute ()
{
  // Here you do everything that needs to be done exactly once for every
  // single file, e.g. collect a list of all lumi-blocks processed

  ANA_MSG_INFO( "=======================");
  ANA_MSG_INFO( "  Calling fileExecute  ");
  ANA_MSG_INFO( "=======================");

  // get TEvent and TStore - must be done here b/c we need to retrieve CutBookkeepers container from TEvent!
  
  xAOD::TEvent *m_event = wk()->xaodEvent();
  xAOD::TStore *m_store = wk()->xaodStore();

  ///////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////// MetaData ////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////

  // get the MetaData tree once a new file is opened, with
 
  TTree* MetaData = dynamic_cast<TTree*>( wk()->inputFile()->Get("MetaData") );
  if ( !MetaData ) {
    ANA_MSG_ERROR( "MetaData tree not found! Exiting.");
    return StatusCode::FAILURE;
  }
  MetaData->LoadTree(0);

  //---------------------------
  // Meta data - CutBookkepers
  //---------------------------
  // Metadata for intial N (weighted) events are used to correctly normalise MC
  // if running on a MC DAOD which had some skimming applied at the derivation stage

  // Now, let's find the actual information
  const xAOD::CutBookkeeperContainer* completeCBC(nullptr);
  if ( !m_event->retrieveMetaInput(completeCBC, "CutBookkeepers").isSuccess() ) {
    ANA_MSG_ERROR("Failed to retrieve CutBookkeepers from MetaData! Exiting.");
    return StatusCode::FAILURE;
  }

  // Find the smallest cycle number, the original first processing step/cycle
  int minCycle(10000);
  for ( auto cbk : *completeCBC ) {
    if ( !(cbk->name().empty())  && (minCycle > cbk->cycle()) ) {
      minCycle = cbk->cycle();
    }
  }

  // Now, let's actually find the right one that contains all the needed info...
  const xAOD::CutBookkeeper* allEventsCBK(nullptr);
  const xAOD::CutBookkeeper* DxAODEventsCBK(nullptr);

  bool m_isDerivation = true;
  //TString m_derivationName = "PHYSKernel";
  //TString m_derivationName = "FTAG1Kernel";
  //TString m_derivationName = "FTAG2Kernel";

  int maxCycle(-1);
  for ( const xAOD::CutBookkeeper* cbk: *completeCBC ) {
    // Find initial book keeper
    ANA_MSG_INFO("Complete cbk name: " << cbk->name() << " - stream: " << cbk->inputStream() );

    if ( cbk->cycle() > maxCycle &&
         cbk->name() == "AllExecutedEvents" &&
         cbk->inputStream() == "StreamAOD" ) {
        allEventsCBK = cbk;
        maxCycle = cbk->cycle();
    }

    // Find derivation book keeper
    if ( m_isDerivation ) {
      if(m_derivationName != "") {
        if ( cbk->name() == m_derivationName )
          DxAODEventsCBK = cbk;
      }
      else if( cbk->name().find("Kernel") != string::npos ) {
        ANA_MSG_INFO("Auto config found DAOD made by Derivation Algorithm: " << cbk->name());
        DxAODEventsCBK = cbk;
      }
    } // is derivation

    // Find and record AOD-level sumW information for all alternate weights
    // The nominal AllExecutedEvents will be filled later, due to posibility of multiple CBK entries
    if ( cbk->name().substr(0, 37) == "AllExecutedEvents_NonNominalMCWeight_" &&
         cbk->name().length() != 17 &&
         cbk->inputStream() == "StreamAOD" ) {
      // Extract weight index from name
      int32_t idx = stoi(cbk->name().substr(37));

      // Fill histogram, making sure that there is space
      // Note will fill bin index = idx+1
      while ( idx >= m_histSumW->GetNbinsX() )
        m_histSumW->LabelsInflate("X");

      m_histSumW->Fill(idx, cbk->sumOfEventWeights());
    }
  }

  if (allEventsCBK == nullptr) {
    ANA_MSG_WARNING("No allEventsCBK found (this is expected for DataScouting, otherwise not). Event numbers set to 0.");
    m_MD_initialNevents     = 0;
    m_MD_initialSumW        = 0;
    m_MD_initialSumWSquared = 0;
  }
  else {
    m_MD_initialNevents     = allEventsCBK->nAcceptedEvents();
    m_MD_initialSumW        = allEventsCBK->sumOfEventWeights();
    m_MD_initialSumWSquared = allEventsCBK->sumOfEventWeightsSquared();
  }

  m_MD_finalNevents	= DxAODEventsCBK->nAcceptedEvents(); 
  m_MD_finalSumW	= DxAODEventsCBK->sumOfEventWeights(); 
  m_MD_finalSumWSquared = DxAODEventsCBK->sumOfEventWeightsSquared();

  // Write metadata event bookkeepers to histogram
  ANA_MSG_INFO( "Meta data from this file:");
  ANA_MSG_INFO( "Initial  events  = "                << static_cast<unsigned int>(m_MD_initialNevents) );
  ANA_MSG_INFO( "Selected events  = "                << static_cast<unsigned int>(m_MD_finalNevents) );
  ANA_MSG_INFO( "Initial  sum of weights = "         << m_MD_initialSumW);
  ANA_MSG_INFO( "Selected sum of weights = "         << m_MD_finalSumW);
  ANA_MSG_INFO( "Initial  sum of weights squared = " << m_MD_initialSumWSquared);
  ANA_MSG_INFO( "Selected sum of weights squared = " << m_MD_finalSumWSquared);

  hist("MetaData_SumW")->Fill(0., m_MD_initialSumW);
  // m_histSumW->Fill(0., m_MD_initialSumW);

  // m_histEventCount -> Fill(1, m_MD_initialNevents);
  // m_histEventCount -> Fill(2, m_MD_finalNevents);
  // m_histEventCount -> Fill(3, m_MD_initialSumW);
  // m_histEventCount -> Fill(4, m_MD_finalSumW);
  // m_histEventCount -> Fill(5, m_MD_initialSumWSquared);
  // m_histEventCount -> Fill(6, m_MD_finalSumWSquared);


  return StatusCode::SUCCESS;
}

StatusCode nTupleMaker :: initialize ()
{
    
  ANA_MSG_INFO( "In initialize" );
  ANA_MSG_INFO( "---------------------------------------------------------------------------------------------------------------------" );
  ANA_MSG_INFO( "----------------------------------------- Analysis parameters -------------------------------------------------------" );
  ANA_MSG_INFO( "Sample: "                  << m_prop_sampleName                );
  ANA_MSG_INFO( "isMC: "                    << m_isMC                           );
  ANA_MSG_INFO( "Final Cuts Only: "         << m_prop_finalOnly                 );
  ANA_MSG_INFO( "Store Tracks: "            << m_prop_storeTracks               );
  ANA_MSG_INFO( "Systematics: "             << m_prop_systematics               );
  ANA_MSG_INFO( "Muon pT cut: "             << m_prop_muonPtCut       << " MeV" );
  ANA_MSG_INFO( "Jet pT cut: "              << m_prop_jetPtCut        << " MeV" );
  // ANA_MSG_INFO( "Leading Jets pT Cut: "     << m_prop_leadingJetPtCut << " MeV" );
  // ANA_MSG_INFO( "Jet Reclustering Size: "   << m_prop_reclusterDeltaR           );
  ANA_MSG_INFO( "Kernel: "                  << m_derivationName                 );
  ANA_MSG_INFO( "Jet tagger ONNX: "         << m_prop_taggerOnnxPath            );
  ANA_MSG_INFO( "---------------------------------------------------------------------------------------------------------------------" );
  ANA_MSG_INFO( "---------------------------------------------------------------------------------------------------------------------" );

  // Initialize Trigger tools
  ANA_CHECK( m_trigConfTool_handle.setProperty("OutputLevel", msg().level()));
  ANA_CHECK( m_trigConfTool_handle.retrieve());

  ANA_CHECK( m_trigDecTool_handle.setProperty( "ConfigTool", m_trigConfTool_handle ));
  ANA_CHECK( m_trigDecTool_handle.setProperty( "TrigDecisionKey", "xTrigDecision" ));
  ANA_CHECK( m_trigDecTool_handle.setProperty( "OutputLevel", msg().level() ));
  ANA_CHECK( m_trigDecTool_handle.setProperty( "NavigationFormat", "TriggerElement") );
  ANA_CHECK( m_trigDecTool_handle.retrieve());

  // Initialize Muon tools
  //// Muon calibration & smearing
  //// Twiki, MuonCP Guidelines    :  https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisGuidelinesR22#Momentum_Calibration
  m_muonCalibTool = new CP::MuonCalibTool("MuonCalibration");
  ANA_CHECK( m_muonCalibTool->setProperty("IsRun3Geo", false) ); // true for run3, false for run2. If you're running on both with the same code, you'll need two separate instances of the tool
  ANA_CHECK( m_muonCalibTool->setProperty("calibMode", CP::MuonCalibTool::correctData_CB)); // see TWiki link above
  ANA_CHECK( m_muonCalibTool->setProperty("doExtraSmearing", false) ); // see TWiki link above; mostly for `HighPt` muons
  ANA_CHECK( m_muonCalibTool->setProperty("do2StationsHighPt", false) ); // see TWiki link above; mostly for `HighPt` muons
  ANA_CHECK( m_muonCalibTool->initialize() ); //initialize the tool
  m_muonIDSelectionTool = new CP::MuonSelectionTool("MuonSelectionID");
  m_muonIDSelectionTool->msg().setLevel( MSG::INFO );
  ANA_CHECK( m_muonIDSelectionTool->setProperty( "MaxEta", 2.5 ) ); //set the properties of the tool as appropriate

  //// Muon ID & selection
  //// 0=Tight, 1=Medium, 2=Loose, 3=VeryLoose (only for debug, not supported), 4=HighPt, 5=LowPtEfficiency
  //// GitLab, MuonSelectionTool.h : https://gitlab.cern.ch/atlas/athena/-/blob/main/PhysicsAnalysis/MuonID/MuonSelectorTools/MuonSelectorTools/MuonSelectionTool.h
  //// TWiki,  MuonCP Guidelines   : https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisGuidelinesR22#WPs_for_Run2
  ANA_CHECK( m_muonIDSelectionTool->setProperty( "MuQuality", 1) ); //set the properties of the tool as appropriate
  ANA_CHECK( m_muonIDSelectionTool->setProperty( "IsRun3Geo", false) ); //true for run3, false for run2. If you're running on both with the same code, you'll need two separate instances of the tool
  ANA_CHECK( m_muonIDSelectionTool->initialize() ); //initialize the tool

  //// Muon isolation
  //// TWiki,  R22 Isolation Rec.  : https://twiki.cern.ch/twiki/bin/view/AtlasProtected/RecommendedIsolationWPsRel22
  m_muonIsoSelectionTool = new CP::IsolationSelectionTool("MuonIsolationSelection");
  ANA_CHECK( m_muonIsoSelectionTool->setProperty("MuonWP","PflowTight_VarRad") ); // PflowTight? PflowLoose?
  ANA_CHECK( m_muonIsoSelectionTool->initialize() ); //initialize the tool

  //// Muon TTVA
  //// GitLab, TTVA.cxx            : https://gitlab.cern.ch/atlas/athena/-/blob/21.2/InnerDetector/InDetRecTools/TrackVertexAssociationTool/Root/TrackVertexAssociationTool.cxx
  //// TWiki,  Run 2 Tracking CP   : https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TrackingCPRecsRun2Final#Track_Vertex_Association_Tool
  m_muonTTVATool = new CP::TrackVertexAssociationTool("MuonTTVATool");
  ANA_CHECK( m_muonTTVATool->setProperty("WorkingPoint", "Muon") );
  ANA_CHECK( m_muonTTVATool->initialize() );

  //// Muon Scale Factors
  //// TODO: Add these back in and validate once PRW is implemented
  m_muonEffToolID = new CP::MuonEfficiencyScaleFactors("MuonIDSFs");
  ANA_CHECK( m_muonEffToolID->setProperty("WorkingPoint","Medium") ); //set up the WP you want the SFs for; you can use other instances of the tool to get different SFs (e.g. Iso)
  ANA_CHECK( m_muonEffToolID->initialize() ); //initialize the tool  
  // m_systematicsTools.push_back(m_muonEffToolID);

  m_muonEffToolIso = new CP::MuonEfficiencyScaleFactors("MuonIsoSFs");
  ANA_CHECK( m_muonEffToolIso->setProperty("WorkingPoint","PflowTight_VarRadIso"));
  ANA_CHECK( m_muonEffToolIso->initialize() ); //initialize the tool  
  // m_systematicsTools.push_back(m_muonEffToolIso);
  
  m_muonEffToolTTVA = new CP::MuonEfficiencyScaleFactors("MuonTTVASFs");
  ANA_CHECK( m_muonEffToolTTVA->setProperty("WorkingPoint","TTVA"));
  ANA_CHECK( m_muonEffToolTTVA->initialize() ); //initialize the tool
  // m_systematicsTools.push_back(m_muonEffToolTTVA);

  m_muonTrigSFTool = new CP::MuonTriggerScaleFactors("MuonTriggerSFs");
  ANA_CHECK( m_muonTrigSFTool->setProperty( "MuonQuality", "Medium" ));
  //ANA_CHECK( m_muonTrigSFTool->setProperty( "CalibrationRelease", "190129_Winter_r21" )); 
  ANA_CHECK( m_muonTrigSFTool->initialize());

  // Initialize JetCalibrationTool
  ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JetCalibrationTool_handle, JetCalibrationTool));
  ANA_CHECK( m_JetCalibrationTool_handle.setProperty("JetCollection", "AntiKt4EMPFlow"));
  ANA_CHECK( m_JetCalibrationTool_handle.setProperty("ConfigFile",    "PreRec_R22_PFlow_ResPU_EtaJES_GSC_February23_230215.config"));
  ANA_CHECK( m_JetCalibrationTool_handle.setProperty("CalibSequence", "JetArea_Residual_EtaJES_GSC"));
  ANA_CHECK( m_JetCalibrationTool_handle.setProperty("CalibArea",     "00-04-82"));
  ANA_CHECK( m_JetCalibrationTool_handle.retrieve());

  // Initialize JetVertexTaggerTool
  ANA_CHECK( m_JVT_tool_handle.setProperty("JetContainer","AntiKt4EMPFlowJets"));
  ANA_CHECK( m_JVT_tool_handle.initialize());

//  ANA_CHECK( ASG_MAKE_ANA_TOOL(m_JVT_tool_handle, CP::JetJvtEfficiency));
//  ANA_CHECK( m_JVT_tool_handle.setProperty("SFFile", "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/JetJvtEfficiency/Moriond2018/JvtSFFile_EMPFlowJets.root" ));
//  ANA_CHECK( m_JVT_tool_handle.retrieve());

  // Initialize Custom Jet-Track Tool
  m_TrackTool = new TrackTool();

// Initialize GNNTool
  ATH_MSG_INFO("Initialising jet tagger with: " + m_prop_taggerOnnxPath);
  map<string, string> nn_remapping = {};
  nn_remapping["BTagTrackToJetAssociator"] = "JetTracks";
  nn_remapping["pcc"] = "pcc";
  nn_remapping["psinglec"] = "psinglec";
  nn_remapping["psingleb"] = "psingleb";
  nn_remapping["pu"] = "pu";
  m_GNNTagger = new FlavorTagDiscriminants::GNN(
    m_prop_taggerOnnxPath,
    FlavorTagDiscriminants::flipTagConfigFromString("STANDARD"),
    nn_remapping,
    FlavorTagDiscriminants::TrackLinkType::IPARTICLE,
    -1.
  );

  // Booking histograms
  // ANA_CHECK( book( TH1F("h_jetPt", "h_jetPt", 100, 0, 500) ) ); // jet pt [GeV]
  // ANA_CHECK( book( TH1F("h_jetEta", "h_jetEta", 100, -6, 6) ) );
  ANA_CHECK( book( TH1F("cutflowHist" ,   "cutflowHist" , n_cuts, 0, n_cuts) ) );
  // ANA_CHECK( book( TH1F("cutflowHistW",   "cutflowHistW", n_cuts, 0, n_cuts) ) );
  ANA_CHECK( book( TH1F("MetaData_SumW", "MetaData_SumW", 1, -0.5, 0.5) ) );
  hist("MetaData_SumW")->SetCanExtend(TH1::kAllAxes);;

  // Get metadata
  ANA_CHECK( requestFileExecute() );
 
  // for (int i=0; i < n_cuts; ++i) {
  //   hist("cutflowHist")->Fill(eventCuts[i], 0);
  // }

  // m_cutflow_all        = 0;
  // m_cutflow_eventClean = 0;
  // m_cutflow_trigger    = 0;
  // m_cutflow_vertex     = 0;
  // m_cutflow_muons      = 0;
  // m_cutflow_muonsQual  = 0;
  // m_cutflow_muonsMZ    = 0;
  // m_cutflow_jets       = 0;
  // m_cutflow_final      = 0;
  m_event_cutLevel     = 0;

  // Booking tree
  ANA_CHECK( book( TTree("analysis", "My analysis ntuple") ) );
  TTree* mytree = tree("analysis");

  // Define vectors

  // Muons
  m_muonPos_pt                           = new vector<float>();
  m_muonPos_eta                          = new vector<float>();
  m_muonPos_phi                          = new vector<float>();
  m_muonPos_e                            = new vector<float>();
  m_muonPos_SF                           = new vector<float>();
  m_muonNeg_pt                           = new vector<float>();
  m_muonNeg_eta                          = new vector<float>();
  m_muonNeg_phi                          = new vector<float>();
  m_muonNeg_e                            = new vector<float>();
  m_muonNeg_SF                           = new vector<float>();

  // Reco Jets
  m_jet_pt                               = new vector<float>();
  m_jet_eta                              = new vector<float>();
  m_jet_phi                              = new vector<float>();
  m_jet_e                                = new vector<float>();
  m_jet_m                                = new vector<float>();
  m_jet_px                               = new vector<float>();
  m_jet_py                               = new vector<float>();
  m_jet_pz                               = new vector<float>();
  m_jet_truthId                          = new vector<int>();
  m_jet_nTracks                          = new vector<int>();

  // Jet tagger scores
  m_jet_pcc                              = new vector<float>();
  m_jet_pc                               = new vector<float>();
  m_jet_pb                               = new vector<float>();
  m_jet_pu                               = new vector<float>();

  // Jet-Tracks (tracks associated to a reco. jet)
  m_jetTrack_pt                          = new vector<vector<double>>();
  m_jetTrack_eta                         = new vector<vector<double>>();
  m_jetTrack_phi                         = new vector<vector<double>>();
  m_jetTrack_e                           = new vector<vector<double>>();
  m_jetTrack_theta                       = new vector<vector<double>>();
  m_jetTrack_d0                          = new vector<vector<double>>();
  m_jetTrack_z0SinTheta                  = new vector<vector<double>>();
  m_jetTrack_dphi                        = new vector<vector<double>>();
  m_jetTrack_deta                        = new vector<vector<double>>();
  m_jetTrack_qOverP                      = new vector<vector<double>>();
  m_jetTrack_IP3D_signed_d0_sig          = new vector<vector<double>>();
  m_jetTrack_IP3D_signed_z0_sig          = new vector<vector<double>>();
  m_jetTrack_phiUncert                   = new vector<vector<double>>();
  m_jetTrack_thetaUncert                 = new vector<vector<double>>();
  m_jetTrack_qOverPUncert                = new vector<vector<double>>();
  m_jetTrack_numPixHits                  = new vector<vector<int>>();
  m_jetTrack_numSCTHits                  = new vector<vector<int>>();
  m_jetTrack_numIPLHits                  = new vector<vector<int>>();
  m_jetTrack_numNIPLHits                 = new vector<vector<int>>();
  m_jetTrack_numIPLSharedHits            = new vector<vector<int>>();
  m_jetTrack_numIPLSplitHits             = new vector<vector<int>>();
  m_jetTrack_numPixSharedHits            = new vector<vector<int>>();
  m_jetTrack_numPixSplitHits             = new vector<vector<int>>();
  m_jetTrack_numSCTSharedHits            = new vector<vector<int>>();
  m_jetTrack_numPixHoles                 = new vector<vector<int>>();
  m_jetTrack_numSCTHoles                 = new vector<vector<int>>();
  m_jetTrack_leptonID                    = new vector<vector<int>>();

  // Truth Jets 
  m_truthJet_pt                          = new vector<float>();
  m_truthJet_eta                         = new vector<float>();
  m_truthJet_phi                         = new vector<float>();
  m_truthJet_e                           = new vector<float>();
  m_truthJet_m                           = new vector<float>();
  m_truthJet_px                          = new vector<float>();
  m_truthJet_py                          = new vector<float>();
  m_truthJet_pz                          = new vector<float>();
  m_truthJet_pdgId                       = new vector<int>();

  // BSM scalar
  m_scalar_pt                            = new vector<float>();
  m_scalar_eta                           = new vector<float>();
  m_scalar_phi                           = new vector<float>();
  m_scalar_e                             = new vector<float>();
  m_scalar_m                             = new vector<float>();
  m_scalar_pdgId                         = new vector<int>();

  // Truth charms
  m_charm_pt                             = new vector<float>();
  m_charm_eta                            = new vector<float>();
  m_charm_phi                            = new vector<float>();
  m_charm_e                              = new vector<float>();
  m_charm_m                              = new vector<float>();
  m_charm_pdgId                          = new vector<int>();
  m_charm_childPt                        = new vector<vector<float>>(); 
  m_charm_childEta                       = new vector<vector<float>>(); 
  m_charm_childPhi                       = new vector<vector<float>>(); 
  m_charm_parentId                       = new vector<vector<int>>(); 

  // Branches
  // Event-level variables
  mytree->Branch("RunNumber",                                &m_runNumber                                      );
  mytree->Branch("ChannelNumber",                            &m_channelNumber                                  );
  mytree->Branch("EventNumber",                              &m_eventNumber                                    );
  mytree->Branch("AverageInteractionsPerBC",                 &m_averageInteractionsPerCrossing                 );
  mytree->Branch("ActualInteractionsPerBC",                  &m_actualInteractionsPerCrossing                  );
  mytree->Branch("MCEventWeight",                            &m_eventWeight                                    );
  mytree->Branch("PileupWeight",                             &m_pileupWeight                                   );
  mytree->Branch("PileupWeightUp",                           &m_pileupWeightUp                                 );
  mytree->Branch("PileupWeightDown",                         &m_pileupWeightDown                               );
  mytree->Branch("TriggerScaleFactor",                       &m_triggerSF                                      );
  mytree->Branch("EventCutLevel",                            &m_event_cutLevel                                 );
  mytree->Branch("CorrectedScaled_AverageInteractionsPerBC", &m_correctedScaled_averageInteractionsPerCrossing );
  mytree->Branch("CorrectedScaled_ActualInteractionsPerBC",  &m_correctedScaled_actualInteractionsPerCrossing  );
  mytree->Branch("Corrected_ActualInteractionsPerBC",        &m_corrected_actualInteractionsPerCrossing        );

  mytree->Branch("TriggerPassed",                            &m_triggerPassed                                  );
  
  // Reco Muons
  mytree->Branch("MuonPos_pt",                               &m_muonPos_pt                                     );
  mytree->Branch("MuonPos_eta",                              &m_muonPos_eta                                    );
  mytree->Branch("MuonPos_phi",                              &m_muonPos_phi                                    );
  mytree->Branch("MuonPos_e",                                &m_muonPos_e                                      );
  mytree->Branch("MuonPos_SF",                               &m_muonPos_SF                                     );
  mytree->Branch("MuonNeg_pt",                               &m_muonNeg_pt                                     );
  mytree->Branch("MuonNeg_eta",                              &m_muonNeg_eta                                    );
  mytree->Branch("MuonNeg_phi",                              &m_muonNeg_phi                                    );
  mytree->Branch("MuonNeg_e",                                &m_muonNeg_e                                      );
  mytree->Branch("MuonNeg_SF",                               &m_muonNeg_SF                                     );

  // Reco Jets
  mytree->Branch("PFlowJet_pt",                              &m_jet_pt                                         );
  mytree->Branch("PFlowJet_eta",                             &m_jet_eta                                        );
  mytree->Branch("PFlowJet_phi",                             &m_jet_phi                                        );
  mytree->Branch("PFlowJet_e",                               &m_jet_e                                          );
  mytree->Branch("PFlowJet_m",                               &m_jet_m                                          );
  mytree->Branch("PFlowJet_px",                              &m_jet_px                                         );
  mytree->Branch("PFlowJet_py",                              &m_jet_py                                         );
  mytree->Branch("PFlowJet_pz",                              &m_jet_pz                                         );
  mytree->Branch("PFlowJet_truthId",                         &m_jet_truthId                                    );
  mytree->Branch("PFlowJet_nTracks",                         &m_jet_nTracks                                    );

  // Jet tagger scores
  mytree->Branch("PFlowJet_pcc",                             &m_jet_pcc                                        );
  mytree->Branch("PFlowJet_pc",                              &m_jet_pc                                         );
  mytree->Branch("PFlowJet_pb",                              &m_jet_pb                                         );
  mytree->Branch("PFlowJet_pu",                              &m_jet_pu                                         );

//  // Jet-Tracks (tracks associated to a reco. jet)
//  mytree->Branch("JetTrack_pt",                              &m_jetTrack_pt                                    );
//  mytree->Branch("JetTrack_eta",                             &m_jetTrack_eta                                   );
//  mytree->Branch("JetTrack_phi",                             &m_jetTrack_phi                                   );
//  mytree->Branch("JetTrack_e",                               &m_jetTrack_e                                     );
//  mytree->Branch("JetTrack_theta",                           &m_jetTrack_theta                                 );
//  mytree->Branch("JetTrack_d0",                              &m_jetTrack_d0                                    );     
//  mytree->Branch("JetTrack_z0SinTheta",                      &m_jetTrack_z0SinTheta                            );
//  mytree->Branch("JetTrack_dphi",                            &m_jetTrack_dphi                                  );
//  mytree->Branch("JetTrack_deta",                            &m_jetTrack_deta                                  );
//  mytree->Branch("JetTrack_qOverP",                          &m_jetTrack_qOverP                                );
//  mytree->Branch("JetTrack_IP3D_signed_d0_sig",              &m_jetTrack_IP3D_signed_d0_sig                    );
//  mytree->Branch("JetTrack_IP3D_signed_z0_sig",              &m_jetTrack_IP3D_signed_z0_sig                    );
//  mytree->Branch("JetTrack_phiUncert"   ,                    &m_jetTrack_phiUncert                             );
//  mytree->Branch("JetTrack_thetaUncert",                     &m_jetTrack_thetaUncert                           );
//  mytree->Branch("JetTrack_qOverPUncert",                    &m_jetTrack_qOverPUncert                          );
//  mytree->Branch("JetTrack_numPixHits",                      &m_jetTrack_numPixHits                            );
//  mytree->Branch("JetTrack_numSCTHits",                      &m_jetTrack_numSCTHits                            );
//  mytree->Branch("JetTrack_numIPLHits",                      &m_jetTrack_numIPLHits                            );
//  mytree->Branch("JetTrack_numNIPLHits",                     &m_jetTrack_numNIPLHits                           );
//  mytree->Branch("JetTrack_numIPLSharedHits",                &m_jetTrack_numIPLSharedHits                      );
//  mytree->Branch("JetTrack_numIPLSplitHits",                 &m_jetTrack_numIPLSplitHits                       );
//  mytree->Branch("JetTrack_numPixSharedHits",                &m_jetTrack_numPixSharedHits                      );
//  mytree->Branch("JetTrack_numPixSplitHits",                 &m_jetTrack_numPixSplitHits                       );
//  mytree->Branch("JetTrack_numSCTSharedHits",                &m_jetTrack_numSCTSharedHits                      );
//  mytree->Branch("JetTrack_numPixHoles",                     &m_jetTrack_numPixHoles                           );
//  mytree->Branch("JetTrack_numSCTHoles",                     &m_jetTrack_numSCTHoles                           );
//  mytree->Branch("JetTrack_leptonID",                        &m_jetTrack_leptonID                              );
//
  // Truth Jets 
  mytree->Branch("TruthJet_pt",                              &m_truthJet_pt                                    );
  mytree->Branch("TruthJet_eta",                             &m_truthJet_eta                                   );
  mytree->Branch("TruthJet_phi",                             &m_truthJet_phi                                   );
  mytree->Branch("TruthJet_e",                               &m_truthJet_e                                     );
  mytree->Branch("TruthJet_m",                               &m_truthJet_m                                     );
  mytree->Branch("TruthJet_px",                              &m_truthJet_px                                    );
  mytree->Branch("TruthJet_py",                              &m_truthJet_py                                    );
  mytree->Branch("TruthJet_pz",                              &m_truthJet_pz                                    );
  mytree->Branch("TruthJet_pdgId",                           &m_truthJet_pdgId                                 );

  // Particles 
  mytree->Branch("TruthScalar_pt",                           &m_scalar_pt                                      );
  mytree->Branch("TruthScalar_eta",                          &m_scalar_eta                                     );
  mytree->Branch("TruthScalar_phi",                          &m_scalar_phi                                     );
  mytree->Branch("TruthScalar_e",                            &m_scalar_e                                       );
  mytree->Branch("TruthScalar_m",                            &m_scalar_m                                       );
  mytree->Branch("TruthScalar_pdgId",                        &m_scalar_pdgId                                   );

  mytree->Branch("Charm_pt",                                 &m_charm_pt                                       );
  mytree->Branch("Charm_eta",                                &m_charm_eta                                      );
  mytree->Branch("Charm_phi",                                &m_charm_phi                                      );
  mytree->Branch("Charm_e",                                  &m_charm_e                                        );
  mytree->Branch("Charm_m",                                  &m_charm_m                                        );
  mytree->Branch("Charm_pdgId",                              &m_charm_pdgId                                    );
  mytree->Branch("Charm_childPt",                            &m_charm_childPt                                  );
  mytree->Branch("Charm_childEta",                           &m_charm_childEta                                 );
  mytree->Branch("Charm_childPhi",                           &m_charm_childPhi                                 );
  mytree->Branch("Charm_parentId",                           &m_charm_parentId                                 );

  int count = 0;

  return StatusCode::SUCCESS;
}


StatusCode nTupleMaker :: execute ()
{
  count ++;
  
  if (count % 1000 == 0) {
    cout << "---Event " << count << endl;
    cout << "isMC " << m_isMC << endl;
  }

  // Clear all vectors
  m_muonPos_pt                    ->clear();
  m_muonPos_eta                   ->clear();
  m_muonPos_phi                   ->clear();
  m_muonPos_e                     ->clear();
  m_muonPos_SF                    ->clear();
  m_muonNeg_pt                    ->clear();
  m_muonNeg_eta                   ->clear();
  m_muonNeg_phi                   ->clear();
  m_muonNeg_e                     ->clear();
  m_muonNeg_SF                    ->clear();
                                  
  m_jet_pt                        ->clear();
  m_jet_eta                       ->clear();
  m_jet_phi                       ->clear();
  m_jet_e                         ->clear();
  m_jet_m                         ->clear();
  m_jet_px                        ->clear();
  m_jet_py                        ->clear();
  m_jet_pz                        ->clear();
  m_jet_truthId                   ->clear();
  m_jet_nTracks                   ->clear();
                                  
  m_jet_pcc                       ->clear();
  m_jet_pc                        ->clear();
  m_jet_pb                        ->clear();
  m_jet_pu                        ->clear();
                                  
  m_jetTrack_pt                   ->clear();
  m_jetTrack_eta                  ->clear();
  m_jetTrack_phi                  ->clear();
  m_jetTrack_e                    ->clear();
  m_jetTrack_theta                ->clear();
  m_jetTrack_d0                   ->clear();     
  m_jetTrack_z0SinTheta           ->clear();
  m_jetTrack_dphi                 ->clear();
  m_jetTrack_deta                 ->clear();
  m_jetTrack_qOverP               ->clear();
  m_jetTrack_IP3D_signed_d0_sig   ->clear();
  m_jetTrack_IP3D_signed_z0_sig   ->clear();
  m_jetTrack_phiUncert            ->clear();
  m_jetTrack_thetaUncert          ->clear();
  m_jetTrack_qOverPUncert         ->clear();
  m_jetTrack_numPixHits           ->clear();
  m_jetTrack_numSCTHits           ->clear();
  m_jetTrack_numIPLHits           ->clear();
  m_jetTrack_numNIPLHits          ->clear();
  m_jetTrack_numIPLSharedHits     ->clear();
  m_jetTrack_numIPLSplitHits      ->clear();
  m_jetTrack_numPixSharedHits     ->clear();
  m_jetTrack_numPixSplitHits      ->clear();
  m_jetTrack_numSCTSharedHits     ->clear();
  m_jetTrack_numPixHoles          ->clear();
  m_jetTrack_numSCTHoles          ->clear();
  m_jetTrack_leptonID             ->clear();
                                  
  m_truthJet_pt                   ->clear();
  m_truthJet_eta                  ->clear();
  m_truthJet_phi                  ->clear();
  m_truthJet_e                    ->clear();
  m_truthJet_m                    ->clear();
  m_truthJet_px                   ->clear();
  m_truthJet_py                   ->clear();
  m_truthJet_pz                   ->clear();
  m_truthJet_pdgId                ->clear();
                                  
  m_scalar_pt                     ->clear();
  m_scalar_eta                    ->clear();
  m_scalar_phi                    ->clear();
  m_scalar_e                      ->clear();
  m_scalar_m                      ->clear();
  m_scalar_pdgId                  ->clear();
  
  m_charm_pt                      ->clear();
  m_charm_eta                     ->clear();
  m_charm_phi                     ->clear();
  m_charm_e                       ->clear();
  m_charm_m                       ->clear();
  m_charm_pdgId                   ->clear();
  m_charm_childPt                 ->clear();
  m_charm_childEta                ->clear();
  m_charm_childPhi                ->clear();
  m_charm_parentId                ->clear();

  //bool eventActive = true;
  m_event_cutLevel = 0;
  hist("cutflowHist")->Fill(eventCuts[0], 1);

  ///////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////// EVENT INFO //////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////

  // Read the EventInfo variables:
  const xAOD::EventInfo* eventInfo = nullptr;
  ANA_CHECK( evtStore()->retrieve(eventInfo, "EventInfo") );

  if (m_isFirstEvent) {

    bool isSim = eventInfo->eventType(xAOD::EventInfo::IS_SIMULATION);

    if (isSim!=m_isMC){ 
      ANA_MSG_ERROR("Wrong data type, specify correctly if data or MC");
      return StatusCode::FAILURE; 
    }

    //int dsid = eventInfo->mcChannelNumber();

    // TODO: Implement systematics
    if (m_isMC) {
      // TODO: Configure the correct lumi & config files for prw
      // lumicalc files as recommended according to:
      // Twiki, GRL Recommendation: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GoodRunListsForAnalysisRun2#Recommendations_for_the_Good_Run
      vector<string> lumicalcFiles1516 = {
        "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root",
        "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root"
      };
      vector<string> lumicalcFiles17   = {
        "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"
      };
      vector<string> lumicalcFiles18   = {
        "/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root"
      };
      
      vector<string> configFiles18;
      vector<string> configFiles17;

      // H->aa->4c Signal Samples, 5 mass points
      configFiles18.push_back("/nTupleMaker/NTUP_PILEUP/sig/mc20_13TeV.601358.PhPy8EG_NNPDF30_A14v3bd_H_a4a4_4c.deriv.NTUP_PILEUP.e8412_s3681_r13145_p4836/NTUP_PILEUP.29919955._000001.pool.root.1");
      configFiles18.push_back("/nTupleMaker/NTUP_PILEUP/sig/mc20_13TeV.601359.PhPy8EG_NNPDF30_A14v3bd_H_a8a8_4c.deriv.NTUP_PILEUP.e8412_s3681_r13145_p4836/NTUP_PILEUP.29919967._000001.pool.root.1");
      configFiles18.push_back("/nTupleMaker/NTUP_PILEUP/sig/mc20_13TeV.601360.PhPy8EG_NNPDF30_A14v3bd_H_a12a12_4c.deriv.NTUP_PILEUP.e8412_s3681_r13145_p4836/NTUP_PILEUP.29919978._000001.pool.root.1");
      configFiles18.push_back("/nTupleMaker/NTUP_PILEUP/sig/mc20_13TeV.601361.PhPy8EG_NNPDF30_A14v3bd_H_a16a16_4c.deriv.NTUP_PILEUP.e8412_s3681_r13145_p4836/NTUP_PILEUP.29919990._000001.pool.root.1");
      configFiles18.push_back("/nTupleMaker/NTUP_PILEUP/sig/mc20_13TeV.601362.PhPy8EG_NNPDF30_A14v3bd_H_a20a20_4c.deriv.NTUP_PILEUP.e8412_s3681_r13145_p4836/NTUP_PILEUP.29920001._000001.pool.root.1");

      // Pythia ttbar non-all-had., single channel, 5 files
      configFiles18.push_back("nTupleMaker/NTUP_PILEUP/bkg/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.NTUP_PILEUP.e6337_s3681_r13145_p4836/NTUP_PILEUP.27121094._000001.pool.root.1");
      configFiles18.push_back("nTupleMaker/NTUP_PILEUP/bkg/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.NTUP_PILEUP.e6337_s3681_r13145_p4836/NTUP_PILEUP.27121130._000001.pool.root.1");
      configFiles18.push_back("nTupleMaker/NTUP_PILEUP/bkg/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.NTUP_PILEUP.e6337_s3681_r13145_p4836/NTUP_PILEUP.27121130._000002.pool.root.1");
      configFiles18.push_back("nTupleMaker/NTUP_PILEUP/bkg/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.NTUP_PILEUP.e6337_s3681_r13145_p4836/NTUP_PILEUP.27121177._000001.pool.root.1");
      configFiles18.push_back("nTupleMaker/NTUP_PILEUP/bkg/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.NTUP_PILEUP.e6337_s3681_r13145_p4836/NTUP_PILEUP.27121243._000001.pool.root.1");

      // Other backgrounds
      configFiles18.push_back("nTupleMaker/NTUP_PILEUP/bkg/mc20_13TeV.364701.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ1WithSW.deriv.NTUP_PILEUP.e7142_s3681_r13145_p4836/NTUP_PILEUP.merged.root");
      configFiles18.push_back("nTupleMaker/NTUP_PILEUP/bkg/mc20_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.NTUP_PILEUP.e3601_s3681_r13145_p4836/NTUP_PILEUP.merged.root");
      configFiles18.push_back("nTupleMaker/NTUP_PILEUP/bkg/mc20_13TeV.361607.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZqqll_mll20.deriv.NTUP_PILEUP.e4711_s3681_r13145_p4836/NTUP_PILEUP.merged.root");
      configFiles18.push_back("nTupleMaker/NTUP_PILEUP/bkg/mc20_13TeV.361610.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZqqll_mqq20mll20.deriv.NTUP_PILEUP.e4711_s3681_r13145_p4836/NTUP_PILEUP.merged.root");
      configFiles18.push_back("nTupleMaker/NTUP_PILEUP/bkg/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.NTUP_PILEUP.e6337_s3681_r13145_p4836/NTUP_PILEUP.merged.root");
      configFiles18.push_back("nTupleMaker/NTUP_PILEUP/bkg/mc20_13TeV.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.deriv.NTUP_PILEUP.e8351_s3681_r13145_p4836/NTUP_PILEUP.merged.root");
      configFiles18.push_back("nTupleMaker/NTUP_PILEUP/bkg/mc20_13TeV.700324.Sh_2211_Zmumu_maxHTpTV2_CFilterBVeto.deriv.NTUP_PILEUP.e8351_s3681_r13145_p4836/NTUP_PILEUP.merged.root");
      configFiles18.push_back("nTupleMaker/NTUP_PILEUP/bkg/mc20_13TeV.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.deriv.NTUP_PILEUP.e8351_s3681_r13145_p4836/NTUP_PILEUP.merged.root");
      configFiles18.push_back("nTupleMaker/NTUP_PILEUP/bkg/mc20_13TeV.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.deriv.NTUP_PILEUP.e8351_s3681_r13144_p4836/NTUP_PILEUP.merged.root");

      configFiles17.push_back("/eos/user/f/fwinkel/imGoingCrazy/backgrounds/DAOD/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.NTUP_PILEUP.e6337_s3681_r13144_p4836/NTUP_PILEUP.27120919._000001.pool.root.1");
      configFiles17.push_back("/eos/user/f/fwinkel/imGoingCrazy/backgrounds/DAOD/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.NTUP_PILEUP.e6337_s3681_r13144_p4836/NTUP_PILEUP.27120962._000001.pool.root.1");
      configFiles17.push_back("/eos/user/f/fwinkel/imGoingCrazy/backgrounds/DAOD/mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.NTUP_PILEUP.e6337_s3681_r13144_p4836/NTUP_PILEUP.33916527._000003.pool.root.1");

       //if ( eventInfo->runNumber() == 284500 ) {
       //  ANA_CHECK( m_prwTool_handle->setProperty("ConfigFiles",   configFiles1516  ) );
       //  ANA_CHECK( m_prwTool_handle->setProperty("LumiCalcFiles", lumicalcFiles1516) );
       //  ANA_CHECK( m_prwTool_handle->initialize() );
       //}
       //else if ( eventInfo->runNumber() == 300000 ) {
       //  ANA_CHECK( m_prwTool_handle->setProperty("ConfigFiles",   configFiles17  ) );
       //  ANA_CHECK( m_prwTool_handle->setProperty("LumiCalcFiles", lumicalcFiles17) );
       //  ANA_CHECK( m_prwTool_handle->initialize() );
       //}
      //if ( eventInfo->runNumber() == 300000 ) {
      if ( eventInfo->runNumber() == 310000 or eventInfo->runNumber() == 300000) {

        ANA_CHECK( m_prwTool_handle.setProperty("ConfigFiles",   configFiles18          ) );
        ANA_CHECK( m_prwTool_handle.setProperty("LumiCalcFiles", lumicalcFiles18        ) );

        //ANA_CHECK( m_prwTool_handle.setProperty("ConfigFiles",   configFiles17          ) );
        //ANA_CHECK( m_prwTool_handle.setProperty("LumiCalcFiles", lumicalcFiles17        ) );

	asg::AnaToolHandle<Trig::ITrigDecisionTool> iTrigDecTool_handle {"Trig::TrigDecisionTool/TrigDecisionTool"};
	//ANA_CHECK( iTrigDecTool_handle.retrieve() );
	//ANA_CHECK( m_prwTool_handle.setProperty("TrigDecisionTool", iTrigDecTool_handle) );
	//ANA_CHECK( m_prwTool_handle.setProperty("TrigDecisionTool", m_trigDecTool_handle) );
	ANA_CHECK( m_prwTool_handle.setProperty("OutputLevel", msg().level()            ) );
        ANA_CHECK( m_prwTool_handle.retrieve() );
      }
      else {
        ANA_MSG_ERROR( "MC Run Number could not be recognized for PRW!" );
      }
    }
  }

  if (m_isFirstEvent) m_isFirstEvent=false;

  if (m_isMC) {

    // Get the PU weight and save as a decoration for other tools to use it	  
    m_prwTool_handle->applySystematicVariation(CP::SystematicSet()).ignore();
    ANA_CHECK(m_prwTool_handle->apply( *eventInfo ));

    // Get corrected Mu vars   
    m_correctedScaled_averageInteractionsPerCrossing = m_prwTool_handle->getCorrectedAverageInteractionsPerCrossing( *eventInfo, true );
    m_corrected_actualInteractionsPerCrossing        = m_prwTool_handle->getCorrectedActualInteractionsPerCrossing(  *eventInfo       );
    m_correctedScaled_actualInteractionsPerCrossing  = m_prwTool_handle->getCorrectedActualInteractionsPerCrossing(  *eventInfo, true );
  }

  static SG::AuxElement::ConstAccessor< char > eventClean_looseBad ("DFCommonJets_eventClean_LooseBad");
  bool jetEventCleaning = eventClean_looseBad(*eventInfo);
  
  if ( !jetEventCleaning ) {return StatusCode::SUCCESS; }

//  if ( jetEventCleaning ) {
//    if (eventActive) {
//      hist("cutflowHist")->Fill(eventCuts[1], 1);
//      m_event_cutLevel++;
//    }
//  }
//  else {
//    if ( m_prop_finalOnly )
//      return StatusCode::SUCCESS;
//    else
//      eventActive = false;
//  }

  ///////////////////////////////////////////////////////////////////////////////////////////
  ///////////////////////// TRIGGERS ////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////

  // Loop over triggers and check if any are passed
  bool passTrig = false;

  auto trigger1 = m_trigDecTool_handle->getChainGroup("HLT_mu26_ivarmedium");
  auto trigger2 = m_trigDecTool_handle->getChainGroup("HLT_mu50");
  passTrig = trigger1->isPassed() || trigger2->isPassed();
  // cout << "DEBUG: Trigger passed " << passTrig << endl;

  if (passTrig) { m_triggerPassed = 1; }
  else{           m_triggerPassed = 0; }

//  if ( passTrig ) {
//    if (eventActive) {
//      //cout<<"Trigger not passed" <<endl;	    
//      hist("cutflowHist")->Fill(eventCuts[2], 1);
//      m_event_cutLevel++;
//    }
//  }
//  else {
//    if ( m_prop_finalOnly )
//      return StatusCode::SUCCESS;
//    else
//      eventActive = false;
//  }

  if ( !passTrig ) {return StatusCode::SUCCESS; }

  ///////////////////////////////////////////////////////////////////////////////////////////
  //////////////////////// Vertices /////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////

  const xAOD::Vertex* pvtx = nullptr;
  const xAOD::VertexContainer *vertices = nullptr;
  ANA_CHECK( evtStore()->retrieve(vertices, "PrimaryVertices") );

  for (unsigned int i = 0; i < vertices->size(); i++) {
    if (vertices->at(i)->vertexType() == xAOD::VxType::PriVtx) {
     pvtx = vertices->at(i);
     break;
    }
  }

  if ( !pvtx ) {return StatusCode::SUCCESS; }

  if (m_isMC) {
    const xAOD::TruthVertex* truth_pvtx = nullptr;
    const xAOD::TruthVertexContainer* truthPVContainer = nullptr;
    ANA_CHECK( evtStore()->retrieve (truthPVContainer, "TruthPrimaryVertices") );
    if (truthPVContainer->size() > 0) {
      truth_pvtx = truthPVContainer->at(0);
    }

    if ( !pvtx or !truth_pvtx ) {return StatusCode::SUCCESS; }

    // If there are reco and truth PVs
    //if ( pvtx && truth_pvtx ) {
    //  if (eventActive) {
    //    hist("cutflowHist")->Fill(eventCuts[3], 1);
    //    m_event_cutLevel++;
    //  }
    //}
    //else {
    //  if ( m_prop_finalOnly )
    //    return StatusCode::SUCCESS;
    //  else
    //    eventActive = false;
    //}
  }

  ///////////////////////////////////////////////////////////////////////////////////////////
  // BEGIN: Muons ///////////////////////////////////////////////////////////////////////////
  ///////////////////////////////////////////////////////////////////////////////////////////

  const xAOD::MuonContainer* uncalib_muons = nullptr;
  ANA_CHECK( evtStore()->retrieve(uncalib_muons, "Muons") );
  pair<xAOD::MuonContainer*, xAOD::ShallowAuxContainer*> muons_shallowCopy = xAOD::shallowCopyContainer(*uncalib_muons);
  xAOD::MuonContainer* muons = muons_shallowCopy.first;

  if ( muons->size()<2 ) {return StatusCode::SUCCESS; }

  //if ( muons->size() >= 2 ) {
  //  if (eventActive) {
  //    hist("cutflowHist")->Fill(eventCuts[4], 1);
  //    m_event_cutLevel++;
  //  }
  //}
  //else {
  //  if ( m_prop_finalOnly )
  //    return StatusCode::SUCCESS;
  //  else
  //    eventActive = false;
  //}

  // Vector for muons that passed selection 
  ConstDataVector <xAOD::MuonContainer> muons_selected(SG::VIEW_ELEMENTS);

  for (xAOD::Muon *muon : *muons) {

    if ( !m_muonCalibTool->applyCorrection(*muon) ) {
      ANA_MSG_ERROR( "Muon calibration/smearing failed!" );
    }

    if (muon->pt() < m_prop_muonPtCut) continue;

    if ( m_muonIDSelectionTool->accept(*muon) ) {
      if ( m_muonIsoSelectionTool->accept(*muon) ) {
        if ( m_muonTTVATool->isCompatible(*(muon->primaryTrackParticle()), *pvtx) ) {
          muons_selected.push_back(muon);      
        }
      }
    }
  }


  if (m_isMC) {
    ANA_CHECK( m_muonTrigSFTool->getTriggerScaleFactor(*muons_selected.asDataVector(), m_triggerSF, "HLT_mu26_ivarmedium_OR_HLT_mu50") );
    //cout << "DEBUG: Muon trigger SF: " << m_triggerSF << endl;
  }

//  if ( muons_selected.size() >= 2 ) {
//    if (eventActive) {
//      hist("cutflowHist")->Fill(eventCuts[5], 1);
//      m_event_cutLevel++;
//    }
//  }
//  else {
//    if ( m_prop_finalOnly )
//      return StatusCode::SUCCESS;
//    else
//      eventActive = false;
//  }

  if ( muons_selected.size() < 2 ) {return StatusCode::SUCCESS;}

  // Divide selected muons into positive & negative sets
  ConstDataVector <xAOD::MuonContainer> muons_pos(SG::VIEW_ELEMENTS);
  ConstDataVector <xAOD::MuonContainer> muons_neg(SG::VIEW_ELEMENTS);

  for (const xAOD::Muon *muon : muons_selected) {
    if (muon->charge() > 0) muons_pos.push_back(muon);
    if (muon->charge() < 0) muons_neg.push_back(muon);
  }

  const xAOD::Muon* muon_pos = nullptr;
  const xAOD::Muon* muon_neg = nullptr;
  double dimuon_mass = 0.;

  for (const xAOD::Muon *muon_p : muons_pos) {
    for (const xAOD::Muon *muon_n : muons_neg) {
      TLorentzVector p1 = muon_p->p4();
      TLorentzVector p2 = muon_n->p4();

      double inv_mass = (p1 + p2).M() / 1e3;

      if ( fabs(inv_mass - 91.1876) < fabs(dimuon_mass - 91.1876) ) {
        muon_pos = muon_p;
        muon_neg = muon_n;
        dimuon_mass = inv_mass;
      }
    }
  }

//  if ( fabs(dimuon_mass - 91.1876) < 10 ) {
//    if (eventActive) {
//      hist("cutflowHist")->Fill(eventCuts[6], 1);
//      m_event_cutLevel++;
//    }
//  }
//  else {
//    if ( m_prop_finalOnly )
//      return StatusCode::SUCCESS;
//    else
//      eventActive = false;
//  }
  if ( !(fabs(dimuon_mass - 91.1876) < 10) ) {return StatusCode::SUCCESS; }

  //////////////////////////////////////////////////////////////////////////////////////
  ////////////////// TRUTH JETS //////////////////////////////////////////////////////// 
  //////////////////////////////////////////////////////////////////////////////////////
  
  const xAOD::JetContainer* truth_jets = nullptr;
  if (m_isMC) {
    ANA_CHECK( evtStore()->retrieve(truth_jets, "AntiKt4TruthDressedWZJets") );
  } 

  //////////////////////////////////////////////////////////////////////////////////////
  ////////////////// RECO JETS ///////////////////////////////////////////////////////// 
  //////////////////////////////////////////////////////////////////////////////////////
  
  const xAOD::JetContainer* uncalib_jets = nullptr;
  ANA_CHECK( evtStore()->retrieve(uncalib_jets, "AntiKt4EMPFlowJets") );
  auto [jets, jet_aux] = xAOD::shallowCopyContainer(*uncalib_jets);

  // Decorate jets with tracks using TrackTool
  // Note that this happens before jet calibration
  const xAOD::TrackParticleContainer* tracks = nullptr;
  ANA_CHECK( evtStore()->retrieve(tracks, "InDetTrackParticles") );
  m_TrackTool->decorateJetsWithTracks(jets, tracks);

  // Tagger evaluation is done with uncalibrated jets as well
  for (int j = 0; j < uncalib_jets->size(); j++) {

    m_GNNTagger->decorate(*jets->at(j));

    // Print statements for debugging / quick checks
    /* cout << "Jet index: " << j << endl
          << "\t Jet Truth Label: " << jets->at(j)->auxdata<int>("HadronConeExclExtendedTruthLabelID")
          << "\t Tagger outputs: "
          << "\t pcc: " << jets->at(j)->auxdecor<float>("pcc")
          << "\t psinglec: " << jets->at(j)->auxdecor<float>("psinglec")
          << "\t psingleb: " << jets->at(j)->auxdecor<float>("psingleb")
          << "\t pu: " << jets->at(j)->auxdecor<float>("pu") << endl;
    */
  }
  
  // Calibrate jets
  ANA_CHECK( m_JetCalibrationTool_handle->applyCalibration(*jets) );
  
  // Jet Selection
  vector<const xAOD::Jet*> jets_selected;
 
  // overlap removal, JVT and truth matching
  for (int j = 0; j < jets->size(); j++) {

    const xAOD::Jet* jet = jets->at(j);

    // pT & eta cut
    if (jet->pt() < m_prop_jetPtCut || fabs(jet->eta()) > 2.5) continue;

    // overlap removal
    // TODO: right now, overlap check is trivially true;
    // needs actual overlap removal tool implementation
    const auto& vetos = m_overlap_checks;
    auto veto_check = [&j=*jet](auto& f) {return f(j); };
    if (any_of(vetos.begin(), vetos.end(), veto_check)) continue;

    // JVT cut
    float jvt = -999;
    jvt = m_JVT_tool_handle->updateJvt(*jet);
    bool fail_jvt = (
      jet->pt() > 20e3 &&
      jet->pt() < 60e3 &&
      abs(jet->eta()) < 2.4 &&
      jvt < 0.5 );
    if (fail_jvt) continue;

    // Truth matching
    if (m_isMC) {
      //int matchedPt = 0;
      float dRmatch = 100;
      for (const xAOD::Jet* tjet : *truth_jets) {
        if (tjet->pt() < 10e3) continue;
        float dr = jet->p4().DeltaR(tjet->p4());
        if (dr < 0.3 && dr < dRmatch) {
          dRmatch   = dr;
          //matchedPt = tjet->pt();
        }
      }
    }  
    // If jet passed every cut
    jets_selected.push_back(jet);
  }

//  if ( jets_selected.size() >= 2 ) {
//    if (eventActive) {
//      hist("cutflowHist")->Fill(eventCuts[7], 1);
//      m_event_cutLevel++;
//    }
//  }
//  else {
//    if ( m_prop_finalOnly )
//      return StatusCode::SUCCESS;
//    else
//      eventActive = false;
//  }

  if ( jets_selected.size() < 2 ) {return StatusCode::SUCCESS;}

  //////////////////////////////////////////////////////////////////////////////////////
  ////////////////// SCALAR BSM BOSON ////////////////////////////////////////////////// 
  //////////////////////////////////////////////////////////////////////////////////////

  bool m_saveCharmInfo = false;

  const DataVector<xAOD::IParticle>* scalars = 0;
  if (m_isMC and m_saveCharmInfo) {
    ANA_CHECK( evtStore()->retrieve(scalars, "TruthBSM") );
  }

//////////////////////////////////////////////////////////////////////////////////////
////////////////// CHARMS //////////////////////////////////////////////////////////// 
//////////////////////////////////////////////////////////////////////////////////////

  const DataVector<xAOD::TruthParticle_v1>* charms = 0;
  if (m_isMC and m_saveCharmInfo) {
    ANA_CHECK( evtStore()->retrieve (charms, "TruthCharm") );
    for (const xAOD::TruthParticle_v1* charm : *charms) {
      // Parent 
      vector<int> charmParentId;
      static SG::AuxElement::ConstAccessor< vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >charmParentsForTagging ("parentLinks");
      if (charmParentsForTagging.isAvailable(*charm)){
        vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > charmParentLinks = charmParentsForTagging( *charm );
        for ( auto link_itr : charmParentLinks ) {
          if( !link_itr.isValid() ) { continue; }
          const xAOD::TruthParticle_v1* charmParent = dynamic_cast<const xAOD::TruthParticle_v1*>( *link_itr );
          charmParentId.push_back( charmParent->pdgId() );
        }
      }
      m_charm_parentId->push_back(charmParentId);
      // Childs
      vector<float> charmChild_eta;
      vector<float> charmChild_phi;
      vector<float> charmChild_pt;
      static SG::AuxElement::ConstAccessor< vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > >charmChildsForTagging ("childLinks");
      if (charmChildsForTagging.isAvailable(*charm)){
        vector<ElementLink<DataVector<xAOD::TruthParticle_v1> > > charmChildLinks = charmChildsForTagging( *charm );
        for ( auto link_itr : charmChildLinks ) {
          if( !link_itr.isValid() ) { continue; }
          const xAOD::TruthParticle_v1* charmChild = dynamic_cast<const xAOD::TruthParticle_v1*>( *link_itr );
          charmChild_pt .push_back( charmChild->pt()  );
          charmChild_eta.push_back( charmChild->eta() );
          charmChild_phi.push_back( charmChild->phi() );
        }
      }
      m_charm_childPt ->push_back(charmChild_pt );
      m_charm_childEta->push_back(charmChild_eta);
      m_charm_childPhi->push_back(charmChild_phi);
    }

  }
  ////////////////////////////////////////////////////////////////////////  
  // If the event got here, it passed every cut and will be selected :) //
  ////////////////////////////////////////////////////////////////////////

//  if (eventActive) {
//    hist("cutflowHist")->Fill(eventCuts[8], 1);
//    m_event_cutLevel++;
//  }

  // Fill event-level variables
  m_runNumber                      = eventInfo->runNumber       ();
  m_eventNumber                    = eventInfo->eventNumber     ();
  m_channelNumber                  = eventInfo->mcChannelNumber ();
  m_averageInteractionsPerCrossing = eventInfo->averageInteractionsPerCrossing();    
  m_actualInteractionsPerCrossing  = eventInfo->actualInteractionsPerCrossing();  
  if ( eventInfo->mcEventWeights().size() > 0 ) {
    m_eventWeight = eventInfo->mcEventWeights().at(0);
  }

  // TODO: Implement systematics of PRW tool
  if (m_isMC) {
    m_pileupWeight = eventInfo->auxdataConst<float>("PileupWeight");
  }

  // Fill muon variables
  float recoidSF = 0, isoSF = 0, ttvaSF = 0;

  if (muon_pos) {
    if ( m_muonEffToolID->getEfficiencyScaleFactor(*muon_pos, recoidSF) != CP::CorrectionCode::Ok ) {
      ANA_MSG_WARNING("m_muonEffToolID couldn't calculate the reco+id SF");
    }
    if ( m_muonEffToolIso->getEfficiencyScaleFactor(*muon_pos, isoSF) != CP::CorrectionCode::Ok ) {
      ANA_MSG_WARNING("m_muonEffToolID couldn't calculate the isolation SF");
    }
    if ( m_muonEffToolTTVA->getEfficiencyScaleFactor(*muon_pos, ttvaSF) != CP::CorrectionCode::Ok ) {
      ANA_MSG_WARNING("m_muonEffToolID couldn't calculate the TTVA SF");
    }

    m_muonPos_pt ->push_back( muon_pos->pt () / 1e3 );
    m_muonPos_eta->push_back( muon_pos->eta() );
    m_muonPos_phi->push_back( muon_pos->phi() );
    m_muonPos_e  ->push_back( muon_pos->e  () / 1e3 );
    m_muonPos_SF ->push_back( recoidSF * isoSF * ttvaSF );
  }
  recoidSF = 0, isoSF = 0, ttvaSF = 0;
  if (muon_neg) {
    if ( m_muonEffToolID->getEfficiencyScaleFactor(*muon_neg, recoidSF) != CP::CorrectionCode::Ok ) {
      ANA_MSG_WARNING("m_muonEffToolID couldn't calculate the reco+id SF");
    }
    if ( m_muonEffToolIso->getEfficiencyScaleFactor(*muon_neg, isoSF) != CP::CorrectionCode::Ok ) {
      ANA_MSG_WARNING("m_muonEffToolID couldn't calculate the isolation SF");
    }
    if ( m_muonEffToolTTVA->getEfficiencyScaleFactor(*muon_neg, ttvaSF) != CP::CorrectionCode::Ok ) {
      ANA_MSG_WARNING("m_muonEffToolID couldn't calculate the TTVA SF");
    }

    m_muonNeg_pt ->push_back( muon_neg->pt () / 1e3 );
    m_muonNeg_eta->push_back( muon_neg->eta() );
    m_muonNeg_phi->push_back( muon_neg->phi() );
    m_muonNeg_e  ->push_back( muon_neg->e  () / 1e3 );
    m_muonNeg_SF ->push_back( recoidSF * isoSF * ttvaSF );
  }

  // Fill reco jet variables
  for (long unsigned int j=0; j < jets_selected.size(); j++) {
    m_jet_pt        ->push_back( jets_selected.at(j)->pt () / 1e3 );
    m_jet_eta       ->push_back( jets_selected.at(j)->eta()       );
    m_jet_phi       ->push_back( jets_selected.at(j)->phi()       );
    m_jet_e         ->push_back( jets_selected.at(j)->e  () / 1e3 );
    m_jet_m         ->push_back( jets_selected.at(j)->m  () / 1e3 );
//    m_jet_px        ->push_back( jets_selected.at(j)->px () / 1e3 );
//    m_jet_py        ->push_back( jets_selected.at(j)->py () / 1e3 );
//    m_jet_pz        ->push_back( jets_selected.at(j)->pz () / 1e3 );
//    m_jet_nTracks   ->push_back( jets_selected.at(j)->auxdecor<int>("NumberOfTracks") );
//    // Tagger scores
//    m_jet_pcc       ->push_back( jets_selected.at(j)->auxdecor<float>("pcc") );
//    m_jet_pc        ->push_back( jets_selected.at(j)->auxdecor<float>("psinglec") );
//    m_jet_pb        ->push_back( jets_selected.at(j)->auxdecor<float>("psingleb") );
//    m_jet_pu        ->push_back( jets_selected.at(j)->auxdecor<float>("pu") );
    // Jet parton origin
    if (m_isMC) { m_jet_truthId->push_back( jets_selected.at(j)->auxdata<int>("HadronConeExclExtendedTruthLabelID") );}
    else        { m_jet_truthId->push_back( -999 ); } 

//    // Jet-Tracks (tracks associated to a reco. jet)
//    if (m_prop_storeTracks) {
//      vector<double> tracks_pt;
//      vector<double> tracks_eta;
//      vector<double> tracks_phi;
//      vector<double> tracks_e;
//
////      for (const auto& jet_track_link : 
////           jets_selected.at(j)->auxdecor< vector<ElementLink<xAOD::IParticleContainer>>>(m_TrackTool->m_jet_track_link) )
////      {
////          // TODO: Actually implement these variables.
////          const xAOD::TrackParticle* jet_track = dynamic_cast<const xAOD::TrackParticle*>( *jet_track_link );
////          tracks_pt                             .push_back( jet_track->pt () / 1e3 );
////          tracks_eta                            .push_back( jet_track->eta() );
////          tracks_phi                            .push_back( jet_track->phi() );
////          tracks_e                              .push_back( jet_track->e  () / 1e3 );
////      }
//
//      m_jetTrack_pt                  ->push_back( tracks_pt       );
//      m_jetTrack_eta                 ->push_back( tracks_eta      );
//      m_jetTrack_phi                 ->push_back( tracks_phi      );
//      m_jetTrack_e                   ->push_back( tracks_e        );
//      // m_jetTrack_theta               ->push_back(  );
//      // m_jetTrack_d0                  ->push_back(  );
//      // m_jetTrack_z0SinTheta          ->push_back(  );
//      // m_jetTrack_dphi                ->push_back(  );
//      // m_jetTrack_deta                ->push_back(  );
//      // m_jetTrack_qOverP              ->push_back(  );
//      // m_jetTrack_IP3D_signed_d0_sig  ->push_back(  );
//      // m_jetTrack_IP3D_signed_z0_sig  ->push_back(  );
//      // m_jetTrack_phiUncert           ->push_back(  );
//      // m_jetTrack_thetaUncert         ->push_back(  );
//      // m_jetTrack_qOverPUncert        ->push_back(  );
//      // m_jetTrack_numPixHits          ->push_back(  );
//      // m_jetTrack_numSCTHits          ->push_back(  );
//      // m_jetTrack_numIPLHits          ->push_back(  );
//      // m_jetTrack_numNIPLHits         ->push_back(  );
//      // m_jetTrack_numIPLSharedHits    ->push_back(  );
//      // m_jetTrack_numIPLSplitHits     ->push_back(  );
//      // m_jetTrack_numPixSharedHits    ->push_back(  );
//      // m_jetTrack_numPixSplitHits     ->push_back(  );
//      // m_jetTrack_numSCTSharedHits    ->push_back(  );
//      // m_jetTrack_numPixHoles         ->push_back(  );
//      // m_jetTrack_numSCTHoles         ->push_back(  );
//      // m_jetTrack_leptonID            ->push_back(  );
//    }
  }
    
  // Fill truth jet variables
  if (m_isMC) {
    for (long unsigned int j=0; j < truth_jets->size(); j++) {
      m_truthJet_pt   ->push_back( truth_jets->at(j)->pt () / 1e3 );
      m_truthJet_eta  ->push_back( truth_jets->at(j)->eta()       );
      m_truthJet_phi  ->push_back( truth_jets->at(j)->phi()       );
      m_truthJet_e    ->push_back( truth_jets->at(j)->e  () / 1e3 );
      m_truthJet_m    ->push_back( truth_jets->at(j)->m  () / 1e3 );
      m_truthJet_px   ->push_back( truth_jets->at(j)->px () / 1e3 );
      m_truthJet_py   ->push_back( truth_jets->at(j)->py () / 1e3 );
      m_truthJet_pz   ->push_back( truth_jets->at(j)->pz () / 1e3 );
      m_truthJet_pdgId->push_back( truth_jets->at(j)->auxdata<int>("PartonTruthLabelID") );
    }
  }
	  
  if (m_isMC and m_saveCharmInfo) {
    for (long unsigned int j=0; j<scalars->size(); j++) {
      m_scalar_pt             ->push_back( scalars->at(j)->pt () / 1e3 );
      m_scalar_eta            ->push_back( scalars->at(j)->eta()       );
      m_scalar_phi            ->push_back( scalars->at(j)->phi()       );
      m_scalar_e              ->push_back( scalars->at(j)->e  () / 1e3 );
      m_scalar_m              ->push_back( scalars->at(j)->m  () / 1e3 );
      m_scalar_pdgId          ->push_back( scalars->at(j)->auxdata<int>("pdgId") );
    }

    // Fill truth charm variables
    for (long unsigned int c=0; c < charms->size(); c++) {
      m_charm_pt              ->push_back( charms->at(c)->pt  () / 1e3 );
      m_charm_eta             ->push_back( charms->at(c)->eta ()       );
      m_charm_phi             ->push_back( charms->at(c)->phi ()       );
      m_charm_e               ->push_back( charms->at(c)->e   () / 1e3 );
      m_charm_m               ->push_back( charms->at(c)->m   () / 1e3 );
      m_charm_pdgId           ->push_back( charms->at(c)->auxdata<int>("pdgId"));
    }
  }

  // Fill the event into the tree:
  tree("analysis")->Fill();

  // Delete stuff
  delete muons_shallowCopy.first;
  delete muons_shallowCopy.second;
  delete jets;
  delete jet_aux;

  //m_isFirstEvent = false;

  return StatusCode::SUCCESS;
}



StatusCode nTupleMaker :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  return StatusCode::SUCCESS;
}


