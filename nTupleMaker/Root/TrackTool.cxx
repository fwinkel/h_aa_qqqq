#include <AnaAlgorithm/AnaAlgorithm.h>
#include <AsgTools/AnaToolHandle.h>
#include <AsgMessaging/MessageCheck.h>

#include "nTupleMaker/TrackTool.h"
#include <xAODJet/Jet.h>
#include <ParticleJetTools/JetParticleShrinkingConeAssociation.h>

#include <xAODBTagging/BTaggingUtilities.h>
#include <FlavorTagDiscriminants/BTagTrackIpAccessor.h>

TrackTool::TrackTool(const std::string& jet_track_link):
  m_acc("btagIp_")
{
  ASG_MAKE_ANA_TOOL(m_JetParticleShrinkingConeAssociation_handle, JetParticleShrinkingConeAssociation);
  m_JetParticleShrinkingConeAssociation_handle.setProperty("InputParticleContainer", "InDetTrackParticles");
  m_JetParticleShrinkingConeAssociation_handle.setProperty("OutputDecoration", "Matched.Jets");
  m_JetParticleShrinkingConeAssociation_handle.setProperty("coneSizeFitPar1", 0.8);
  m_JetParticleShrinkingConeAssociation_handle.setProperty("coneSizeFitPar2", -1e300);
  m_JetParticleShrinkingConeAssociation_handle.setProperty("coneSizeFitPar3", 0);
  m_JetParticleShrinkingConeAssociation_handle.retrieve();

  m_jet_track_link = jet_track_link;
}

void TrackTool::decorateJetsWithTracks(const xAOD::JetContainer* jets, const xAOD::TrackParticleContainer* tracks) {
  const std::vector<std::vector<ElementLink< xAOD::IParticleContainer>>> *jet_track_links;
  jet_track_links = m_JetParticleShrinkingConeAssociation_handle->match( *jets, *tracks );

  for (int j = 0; j < jets->size(); j++) {
    int n_tracks = 0;
    std::vector<ElementLink<xAOD::IParticleContainer>> jet_tracks;

    for (const auto& track_link : jet_track_links->at(j)) {
      if (!track_link.isValid()) {
        throw std::logic_error("Invalid IParticle link in jet-track association!");
      }

      const xAOD::TrackParticle* track = dynamic_cast<const xAOD::TrackParticle*>( *track_link );

      if (!track) {
        throw std::logic_error("Failed to cast IParticle to TrackParticle!");
      }

      if (passTrackSelection(*track)) {
        jet_tracks.push_back(track_link);
        n_tracks++;
      }

    }

    jets->at(j)->auxdecor<std::vector<ElementLink<xAOD::IParticleContainer>>>("JetTracks") = jet_tracks;
    jets->at(j)->auxdecor<int>("NumberOfTracks") = n_tracks;
  }

  delete jet_track_links;
}

bool TrackTool::passTrackSelection(const xAOD::TrackParticle& tp) const
{
  static SG::AuxElement::ConstAccessor<unsigned char> pix_hits("numberOfPixelHits");
  static SG::AuxElement::ConstAccessor<unsigned char> pix_holes("numberOfPixelHoles");
  static SG::AuxElement::ConstAccessor<unsigned char> pix_shared("numberOfPixelSharedHits");
  static SG::AuxElement::ConstAccessor<unsigned char> pix_dead("numberOfPixelDeadSensors");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_hits("numberOfSCTHits");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_holes("numberOfSCTHoles");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_shared("numberOfSCTSharedHits");
  static SG::AuxElement::ConstAccessor<unsigned char> sct_dead("numberOfSCTDeadSensors");

  if (std::abs(tp.eta()) > 2.5)
    return false;

  double n_module_shared = (pix_shared(tp) + sct_shared(tp)) / 2;
  if (n_module_shared > 999)
    return false;

  if (tp.pt() <= 500)
    return false;

  if (std::isfinite(5) &&
      std::abs(m_acc.d0(tp)) >= 5)
    return false;

  if (std::isfinite(5) &&
      std::abs(m_acc.z0SinTheta(tp)) >= 5)
    return false;

  if (pix_hits(tp) + pix_dead(tp) + sct_hits(tp) + sct_dead(tp) < 8)
    return false;

  if ((pix_holes(tp) + sct_holes(tp)) > 2)
    return false;

  if (pix_holes(tp) > 1)
    return false;

  return true;
}


