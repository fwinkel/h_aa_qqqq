#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = "submitDir",
                   help = "Submission directory for EventLoop" )
parser.add_option( '--input-path', dest = 'input_path',
                   action = 'store', type = 'string', default = "",
                   help = "Path to directory containing input DAOD.root files" )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )

if options.input_path:
    ROOT.SH.ScanDir().filePattern( "DAOD*.root*" ).scan( sh, options.input_path )
else:    
    # If input path is not specified, use 4 GeV a->cc sample in Sanha's EOS
    inputFilePath = '/afs/cern.ch/user/s/scheong/eos/data_storage/h_aa_qqqq/AOD/recent/sig/mc20_13TeV.601358.PhPy8EG_NNPDF30_A14v3bd_H_a4a4_4c.deriv.DAOD_FTAG1.e8412_s3681_r13145_p5922/'
    ROOT.SH.ScanDir().filePattern( "DAOD*.root*" ).scan( sh, inputFilePath )
    # ROOT.SH.ScanDir().filePattern( "DAOD_FTAG1.37035199._000002.pool.root.1" ).scan( sh, inputFilePath )
    # ROOT.SH.ScanDir().filePattern( "DAOD_FTAG1.37035199._000003.pool.root.1" ).scan( sh, inputFilePath )
    # ROOT.SH.ScanDir().filePattern( "DAOD_FTAG1.37035199._000004.pool.root.1" ).scan( sh, inputFilePath )

sh.printContent()
#sh_hist = ROOT.SH.SampleHandler()
#sh_hist.load (options.submission_dir + '/output-ANALYSIS')

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, -1 )
# job.options().setDouble( ROOT.EL.Job.optMaxEvents, 30000 )
# job.options().setDouble( ROOT.EL.Job.optMaxEvents, 1000 )
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Adding algorithm sequence

# Set data type to MC
dataType = "mc"

from nTupleMaker.nTupleMakerAlgorithms import makeSequence
algSeq = makeSequence (dataType)
# print(algSeq) # For debugging
for alg in algSeq:
    job.algsAdd( alg )
    pass

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg = createAlgorithm ( 'nTupleMaker', 'AnalysisAlg' )
alg.sampleName    = 'H_aa_cccc'
alg.finalOnly     = False
alg.storeTracks   = True
# alg.muonPtCut     = 10000.0
# alg.jetPtCut      = 20000.0
# alg.leadingJetPtCut = 40000.0
# alg.reclusterDeltaR = 0.6

# later on we'll add some configuration options for our algorithm that go here

# Add our algorithm to the job
job.algsAdd( alg )
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )


