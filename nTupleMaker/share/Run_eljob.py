#!/usr/bin/env python
import argparse
import os, sys
import shutil
import json
import subprocess
import tempfile
import uuid
from datetime import datetime

import ROOT

def main(args):
    if args.grid:
        grid_name = os.getenv('RUCIO_ACCOUNT')
        print("Submitting job under grid name: %s" % grid_name)
    else:
        print("Running job locally")
        grid_name = "local"

    now = datetime.now().strftime('%Y%m%d-%H%M')

    # Set up (Py)ROOT.
    ROOT.xAOD.Init().ignore()
    
    # Set up the sample handler object. See comments from the C++ macro
    # for the details about these lines.
    sh = ROOT.SH.SampleHandler()
    sh.setMetaString( 'nc_tree', 'CollectionTree' )

    if args.grid:
        ROOT.SH.scanRucio(sh, args.inputSample)
    else:
        ROOT.SH.ScanDir().filePattern( "DAOD*.root.*" ).scan( sh, args.inputSample )

    sh.printContent()

    #sh_hist = ROOT.SH.SampleHandler()
    #sh_hist.load (options.submission_dir + '/output-ANALYSIS')
    
    # Create an EventLoop job.
    job = ROOT.EL.Job()
    job.sampleHandler( sh )
    #job.options().setDouble( ROOT.EL.Job.optMaxEvents, args.maxEvents )
    #job.options().setDouble( ROOT.EL.Job.optMaxEvents, 2000 )
    job.options().setDouble( ROOT.EL.Job.optMaxEvents, 20000 )
    job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')
        
    # Set data type to MC
    dataType = ""
    _isMC = args.isMC

    if   _isMC==True:  dataType = "mc"
    elif _isMC==False: dataType = "data"
    else: 
        print("")
        print("Data type not specified, exiting")
        print("")
        sys.exit(1)

    print(dataType)
    print(dataType)
    print(dataType)
    print(dataType)
    print(dataType)

    from nTupleMaker.nTupleMakerAlgorithms import makeSequence
    algSeq = makeSequence (dataType)
    # print(algSeq) # For debugging
    for alg in algSeq:
        job.algsAdd( alg )
        pass
    
    # Create the algorithm's configuration.
    from AnaAlgorithm.DualUseConfig import createAlgorithm
    alg = createAlgorithm ( "nTupleMaker", "AnalysisAlg" )
    alg.isMC           = _isMC
    alg.sampleName     = args.sampleName
    alg.derivationName = args.derivationName
    alg.finalOnly      = args.finalOnly
    alg.storeTracks    = args.storeTracks
    alg.systematics    = args.systematics
    alg.muonPtCut      = args.muonPtCut
    alg.jetPtCut       = args.jetPtCut
    alg.taggerOnnxPath = args.taggerOnnxPath

    # Add our algorithm to the job
    job.algsAdd( alg )
    job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

    if args.dryRun:
        print(args)
    else:
        if args.grid:
            driver = ROOT.EL.PrunDriver()
            driver.options().setString(
                "nc_outputSampleName",
                "user.%s.%s.%s" % (grid_name, args.sampleName, now)
            )
            #driver.submitOnly( job, options.submission_dir )
            driver.submitOnly( job, "%s.%s" % (args.sampleName, now) )
        else:
            # Run the job using the direct driver.
            driver = ROOT.EL.DirectDriver()
            driver.submit( job, "%s.%s" % (args.sampleName, now) )
        

if __name__ == "__main__" :
    parser = argparse.ArgumentParser()

    parser.add_argument("--inputSample", type=str,
                        help="Specify an input sample. If `grid` is True, the DSID is searched for via Rucio. If `grid` is False, this must be a path to a local directory.")

    # this is fed as an input into the nTupleMaker algorithm config

    parser.add_argument("--sampleName", type=str,
                        help="Specify a name for the output sample.")

    parser.add_argument("--derivationName", type=str, default="PHYSKernel",
                        help="Specify a name for the cbkkpr Kernel. The options are PHYSKernel, FTAG1Kernel or FTAG2Kernel")

    parser.add_argument("--isMC", action="store_true", 
                        help="True if running over MC, False if running over data")

    parser.add_argument("--submitDir", type=str, default="submitDir",
                        help="Job submission directory.")

    parser.add_argument("--grid", action="store_true",
                        help="If True, submit nTupleMaker jobs to the grid. If False, run the nTupleMaker locally.")

    parser.add_argument("--dryRun", action="store_true",
                        help="Don't actually execute anything, only print the arguments.")

    # parser.add_argument("--sampleType", choices=["data", "signal", "bkg"],
    #                     help="Types of samples to process. The options are \"data\", \"signal\", and \"bkg\"")

    parser.add_argument("--maxEvents", type=int, default=-1,
                        help="Maximum # of events to iterate through.")

    # arguments below this line are fed into the nTupleMaker algorithm config
    parser.add_argument("--finalOnly", action="store_true",
                        help="Only the events passing all cuts are stored into the output ntuple. False by default.")

    parser.add_argument("--storeTracks", action="store_true",
                        help="Store jet-tracks into the output ntuple. False by default")

    parser.add_argument("--systematics", type=int, default=0,
                        help="Specify which systematic variations to run. If not specified, only nominal setting is run")

    parser.add_argument("--muonPtCut", type=float, default=10000.,
                        help="Muon pT cut in MeV (after calibration)")

    parser.add_argument("--jetPtCut", type=float, default=20000.,
                        help="Jet pT cut in MeV (after calibration)")

    parser.add_argument("--taggerOnnxPath", type=str, default="nTupleMaker/tagger/GN2-S3_20231028-T164149/network.onnx",
                        help="Path to the custom GN* tagger.onnx file")

    main(parser.parse_args())
