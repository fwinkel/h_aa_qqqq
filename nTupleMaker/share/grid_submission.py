#!/usr/bin/python
import os, sys
# from time import strftime

# timestamp = strftime("_%d%m%y")
# reclusteringR = 0.8

sig_samples = {
  # Current H->aa->4c signal samples
  "mc20e.H_a4a4_4c.nominal.FTAG1"   : "mc20_13TeV.601358.PhPy8EG_NNPDF30_A14v3bd_H_a4a4_4c.deriv.DAOD_FTAG1.e8412_s3681_r13145_p5922",
  "mc20e.H_a8a8_4c.nominal.FTAG1"   : "mc20_13TeV.601359.PhPy8EG_NNPDF30_A14v3bd_H_a8a8_4c.deriv.DAOD_FTAG1.e8412_s3681_r13145_p5922",
  "mc20e.H_a12a12_4c.nominal.FTAG1" : "mc20_13TeV.601360.PhPy8EG_NNPDF30_A14v3bd_H_a12a12_4c.deriv.DAOD_FTAG1.e8412_s3681_r13145_p5922",
  "mc20e.H_a16a16_4c.nominal.FTAG1" : "mc20_13TeV.601361.PhPy8EG_NNPDF30_A14v3bd_H_a16a16_4c.deriv.DAOD_FTAG1.e8412_s3681_r13145_p5922",
  "mc20e.H_a20a20_4c.nominal.FTAG1" : "mc20_13TeV.601362.PhPy8EG_NNPDF30_A14v3bd_H_a20a20_4c.deriv.DAOD_FTAG1.e8412_s3681_r13145_p5922",
}

bkg_samples = {
  # Pythia ttbar, non-all-hadronic
  "mc20e.Py_ttbar.nominal.FTAG1"       : "mc20_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_FTAG1.e6337_s3681_r13145_p5922",
  # # Pythia Z+jets
  # # Currently in production, as of 2024/02/13
  # "mc20e.Py_Zmumu.nominal.FTAG1"       : "mc20_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.DAOD_FTAG1.e3601_s3681_r13145_p5922",
  # # Sherpa Z+jets, divided into jet flavors
  # # Currently in production, as of 2024/02/13
  # "mc20e.Sh_Zmumu_BF.nominal.FTAG1"    : "mc20_13TeV.700323.Sh_2211_Zmumu_maxHTpTV2_BFilter.deriv.DAOD_FTAG1.e8351_s3681_r13145_p5922",
  # "mc20e.Sh_Zmumu_CFBV.nominal.FTAG1"  : "mc20_13TeV.700324.Sh_2211_Zmumu_maxHTpTV2_CFilterBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13145_p5922",
  # "mc20e.Sh_Zmumu_CVBV.nominal.FTAG1"  : "mc20_13TeV.700325.Sh_2211_Zmumu_maxHTpTV2_CVetoBVeto.deriv.DAOD_FTAG1.e8351_s3681_r13145_p5922",
  # # Pythia VV->qqll samples
  # # Currently in production, as of 2024/02/13
  # "mc20e.Py_WZ_qqll.nominal.FTAG1"     : "mc20_13TeV.361607.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_WZqqll_mll20.deriv.DAOD_FTAG1.e4711_s3681_r13145_p5922",
  # "mc20e.Py_ZZ_qqll.nominal.FTAG1"     : "mc20_13TeV.361610.PowhegPy8EG_CT10nloME_AZNLOCTEQ6L1_ZZqqll_mqq20mll20.deriv.DAOD_FTAG1.e4711_s3681_r13145_p5922",
}


systematics = {
  0: "nominal"
}

cmd_base = "Run_eljob.py --sampleName %s --inputSample %s --grid --storeTracks"

for name, sample in sig_samples.items():
  cmd = cmd_base % (name, sample)
  for i_syst, syst in systematics.items():
    if i_syst:
      cmd.replace("nominal", syst)

    print(cmd)
    # os.system(command)

for name, sample in bkg_samples.items():
  cmd = cmd_base % (name, sample)
  for i_syst, syst in systematics.items():
    if i_syst:
      cmd.replace("nominal", syst)

    print(cmd)
    # os.system(command)