from AnaAlgorithm.AlgSequence import AlgSequence
from AnaAlgorithm.DualUseConfig import createAlgorithm, createService, addPrivateTool

def makeSequence (dataType) :
 
    #triggerChains = [".+"]
    triggerChains = [
        'HLT_mu26_ivarmedium',
        'HLT_mu50'
    ]

    algSeq = AlgSequence()

    # Set up the systematics loader/handler service:
#    sysService = createService( 'CP::SystematicsSvc', 'SystematicsSvc', sequence = algSeq )
#    sysService.sigmaRecommended = 1
#
#    # Include and set up the pileup analysis sequence:
#    from AsgAnalysisAlgorithms.PileupAnalysisSequence import makePileupAnalysisSequence
#    from AsgAnalysisAlgorithms.AsgAnalysisAlgorithmsTest import pileupConfigFiles
#    prwfiles, lumicalcfiles = pileupConfigFiles( dataType )
#    pileupSequence = makePileupAnalysisSequence( dataType,
#                                                 userPileupConfigs=prwfiles,
#                                                 userLumicalcFiles=lumicalcfiles,
#                                               )
#    pileupSequence.configure( inputName = "EventInfo", outputName = "" )
#
#    # Add the pileup analysis sequence to the job:
#    algSeq += pileupSequence


    ## Include and set up the trigger analysis sequence:
    #from TriggerAnalysisAlgorithms.TriggerAnalysisSequence import makeTriggerAnalysisSequence
    ##triggerSequence = makeTriggerAnalysisSequence( dataType, triggerChains=triggerChains,[],True)
    #triggerSequence = makeTriggerAnalysisSequence( dataType, triggerChains, True)

    # Add the trigger analysis sequence to the job:
    #algSeq += triggerSequence

#    # Include and set up the electron analysis sequence:
#    from EgammaAnalysisAlgorithms.ElectronAnalysisSequence import makeElectronAnalysisSequence
#    workingpoint = 'LooseLHElectron.Loose_VarRad'
#    electronSequence = makeElectronAnalysisSequence( dataType, workingpoint, postfix = 'loose',
#                                                     recomputeLikelihood=False, shallowViewOutput = False )
#    electronSequence.configure( inputName = 'Electrons',
#                                outputName = 'AnaElectrons_%SYS%' )
#    
#    # Add the electron analysis sequence to the job:
#    algSeq += electronSequence

#    electronMinPt = 27e3 # Minimum pt in MeV
#    electronMaxEta = 2.47
#
#    # Schedule selection algorithms for all objects
#
#    # Include and set up electron selection algorithm:
#    selAlgEl = createAlgorithm( 'CP::AsgSelectionAlg', 'UserElectronSelectionAlg' )
#    addPrivateTool( selAlgEl, 'selectionTool', 'CP::AsgPtEtaSelectionTool' )
#    if electronMinPt is not None :
#        selAlgEl.selectionTool.minPt = electronMinPt
#    if electronMaxEta is not None :
#        selAlgEl.selectionTool.maxEta = electronMaxEta
#    selAlgEl.selectionDecoration = 'selectPtEta,as_char'
#    selAlgEl.particles = 'AnaElectrons_%SYS%'
#
#    # Add the electron selection algorithm to the job:
#    algSeq += selAlgEl
#
#    # Include and set up the muon analysis sequence:
#    from MuonAnalysisAlgorithms.MuonAnalysisSequence import makeMuonAnalysisSequence
#    workingpoint = 'Medium.NonIso'
#    muonSequence = makeMuonAnalysisSequence( dataType, workingpoint, postfix = 'medium',
#                                             shallowViewOutput = False )
#    muonSequence.configure( inputName = 'Muons',
#                            outputName = 'AnaMuons_%SYS%' )
#
#    # Add the muon analysis sequence to the job:
#    algSeq += muonSequence
#
#    muonMinPt = 25e3 # Minimum pt in MeV
#    muonMaxEta = None
#
#    # Include and set up muon selection algorithm:
#    selAlgMu = createAlgorithm( 'CP::AsgSelectionAlg', 'UserMuonSelectionAlg' )
#    addPrivateTool( selAlgMu, 'selectionTool', 'CP::AsgPtEtaSelectionTool' )
#    if muonMinPt is not None :
#        selAlgMu.selectionTool.minPt = muonMinPt
#    if muonMaxEta is not None :
#        selAlgMu.selectionTool.maxEta = muonMaxEta
#    selAlgMu.selectionDecoration = 'selectPtEta,as_char'
#    selAlgMu.particles = 'AnaMuons_%SYS%'
#
#    # Add the muon selection algorithm to the job:
#    algSeq += selAlgMu

    # Include and set up the jet analysis sequence:
#    from JetAnalysisAlgorithms.JetAnalysisSequence import makeJetAnalysisSequence
#    jetContainer = 'AntiKt4EMPFlowJets'
#    jetSequence = makeJetAnalysisSequence( dataType, jetContainer,
#                                           runGhostMuonAssociation = True, shallowViewOutput = False )
#    jetSequence.configure( inputName = jetContainer,
#                           outputName = 'AnaJets_%SYS%' )
#
#    # Add the jet analysis sequence to the job:
#    algSeq += jetSequence

    #jetMinPt = 20e3 # Minimum pt in MeV
    #jetMaxEta = 4.5

    ## Include and set up jet selection algorithm:
    #selAlgJet = createAlgorithm( 'CP::AsgSelectionAlg', 'UserJetSelectionAlg' )
    #addPrivateTool( selAlgJet, 'selectionTool', 'CP::AsgPtEtaSelectionTool' )
    #if jetMinPt is not None :
    #    selAlgJet.selectionTool.minPt = jetMinPt
    #if jetMaxEta is not None :
    #    selAlgJet.selectionTool.maxEta = jetMaxEta
    #selAlgJet.selectionDecoration = 'selectPtEta'
    #selAlgJet.particles = 'AnaJets_%SYS%'

    ## Add the jet selection algorithm to the job:
    #algSeq += selAlgJet

    # 2024/01/23: Sanha testing jet-track association tool
    

    return algSeq
