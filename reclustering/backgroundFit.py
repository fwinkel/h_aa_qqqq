import os
import ROOT
import numpy as np
import matplotlib.pyplot as plt

######### Parameters ###############
lumi     = 1
xsec_b   = 1.9 #nb
R        = "R=0.6"

radii    = [
            "R=0.4",
            "R=0.5",
            "R=0.6",
            "R=0.7",
            "R=0.8"
           ]
######### Names #################################
pdfName = "bFit.pdf"
srootFile = "./signal_invariantMassHistos.root" 
brootFile = "./backgr_invariantMassHistos.root"
#################################################

def backgroundFit(xsec_s, r, c, name):
  # Inputs
  # Signal
  sFile     = ROOT.TFile().Open(srootFile)
  s         = sFile.Get(r)
  sSOW      = sFile.Get("MetaData_SumW").GetBinContent(1)
  sf_s      = lumi*xsec_s/sSOW
  s.Scale(sf_s)
  
  # Background
  bFile     = ROOT.TFile().Open(brootFile)
  b         = bFile.Get(r)
  bSOW      = bFile.Get("MetaData_SumW").GetBinContent(1)
  sf_b      = lumi*xsec_b/bSOW
  b.Scale(sf_b)
  
  # Background Model
  fitRange = [75,300]
  backgroundModel = ROOT.TF1('zjets', '[0]*((1-x/13600)^[1])*( pow( x/13600,  -[2] - [3]*log(x/13600) - [4]*pow(log(x/13600),2) ) )', fitRange[0], fitRange[1])

  # Set/fix parameters to avoid STATUS=NOT POSDEF when increasing number of calls 
  backgroundModel.FixParameter(0,  5.7e-07) 
  #backgroundModel.SetParameter(0,  5.7e-07) 

  ## Canvas
  c.cd()
  b.SetStats(0)
  b.SetTitle("Dijet invariant mass")
  b.SetLineColor(ROOT.kBlack)
  b.GetYaxis().SetTitle("Number of entries (Scaled)")
  ROOT.gPad.Update()

  # Fit background
  ROOT.Math.MinimizerOptions.SetDefaultMaxFunctionCalls(100000)  # Increase number of calls to avoid STATUS=CALL LIMIT
  ROOT.Math.MinimizerOptions.SetDefaultTolerance(1) 
  fitResult = b.Fit(backgroundModel, "R")
  f    = b.GetFunction('zjets')
  chi2 = f.GetChisquare()
  ndof = f.GetNDF()
  
  # Legend
  leg = ROOT.TLegend(.65,.7,.9,.9)
  leg.AddEntry(b,"Signal+Background")
  leg.AddEntry(f,"Background only fit")
  leg.AddEntry(b,"#chi^{2} = "+str(round(chi2,3)), "-")
  leg.AddEntry(b,"N_{dof} = "+str(ndof),"-")
  
  # S+B
  b.Add(s)

  # Drawing
  b.Draw()
  leg.Draw('SAME')
  f.Draw("SAME")
 
  # Fix Y axis range 
  ROOT.gPad.SetLogy()
  b.GetYaxis().SetRangeUser(1e-7, 5*1e-3)
  ROOT.gPad.Update()
 
  # Ticks at top and right
  ROOT.gPad.SetTicks(1);
  ROOT.gPad.Update()
 
  # Signal region
  binMax     = s.GetMaximumBin()
  peakXCoord = s.GetXaxis().GetBinCenter(binMax)
  
  xcoord1 = peakXCoord-20  
  xcoord2 = peakXCoord+20 
  ycoord1 = f.Eval(xcoord1)
  ycoord2 = f.Eval(xcoord2)
  line1  = ROOT.TLine(xcoord1, 0, xcoord1, ycoord1)
  line2  = ROOT.TLine(xcoord2, 0, xcoord2, ycoord2)
  line1.SetLineStyle(7)
  line2.SetLineStyle(7)
  line1.SetLineColor(ROOT.kOrange+2)
  line2.SetLineColor(ROOT.kOrange+2)
  line1.Draw('SAME')
  line2.Draw('SAME')

  # Estimate residual
  D = b.Integral(b.FindBin(xcoord1), b.FindBin(xcoord2), 'width')    
  B = f.Integral(xcoord1, xcoord2)
  res = (D-B)/np.sqrt(B)

  # ATLAS
  x = 0.45;
  y = 0.4;
 
  jets = 'null'
  if (r=="R=0.4"):
      jets = "AntiKt4EMPFlow Jets"
  else:
      jets = f"{r} reclustered AntiKtEMPFlow Jets"

  ATLAS = ROOT.TLatex();
  ATLAS.SetNDC();
  ATLAS.SetTextSize(0.05);
  ATLAS.SetTextFont(72);
  ATLAS.DrawLatex(x, y, "ATLAS");
  ATLAS.SetTextSize(0.04);
  ATLAS.SetTextFont(42);
  ATLAS.DrawLatex(x, y, "                  Simulation");
  ATLAS.SetTextSize(0.03);
  ATLAS.DrawLatex(x, y - 0.03, "#sqrt{s} = 13 TeV, H #rightarrow aa #rightarrow cccc vs Z+Jets");
  ATLAS.SetTextSize(0.03);
  ATLAS.DrawLatex(x, y - 0.06, "m_{a} = 20 GeV");
  ATLAS.SetTextSize(0.03);
  ATLAS.DrawLatex(x, y - 0.09, jets);
  ATLAS.SetTextSize(0.03);
  ATLAS.DrawLatex(x, y - 0.12, "#sigma_{H #rightarrow aa #rightarrow cccc} = %.3f nb"%xsec_s);
  ATLAS.SetTextSize(0.03);
  ATLAS.DrawLatex(x,y-0.17,"(D-B)/#sqrt{B} = "+str(round(res,4)))
  #ATLAS.DrawLatex(x,y-0.17,"#frac{(D-B)}{#sqrt{B}} = "+str(round(res,4)))

  return c.Print(name), res 



# Create canvas to plot fits
can = ROOT.TCanvas()
can.Print(pdfName+"[")

# Define aux vars
labels = []
maxVal = 0

# Loop in signal xsec
for xs in np.linspace(0.001,0.3,5): 
    # Loop in radii
    X = [] 
    Y = [] 
    for r in range(len(radii)):
        rad = -999
        res = -888
        # This calls the function that returns the canvas and gets the residual (sensitivity) 
        res = backgroundFit(xs, radii[r], can, pdfName)[1]
        if (radii[r] == "R=0.4"): rad = 0.4
        if (radii[r] == "R=0.5"): rad = 0.5
        if (radii[r] == "R=0.6"): rad = 0.6
        if (radii[r] == "R=0.7"): rad = 0.7
        if (radii[r] == "R=0.8"): rad = 0.8
        X.append(rad)
        Y.append(res)
        if max(Y) > maxVal: maxVal = max(Y)
    # Plot residual vs R for different xsex 
    plt.plot(X, Y, 'o-')
    labels.append(r'$\sigma_{H \rightarrow aa \rightarrow cccc}$ = %.3f nb' % xs)
can.Print(pdfName+"]")

# Plot aesthethics
plt.xlabel(r'$\Delta$R', fontsize=14.5)
plt.ylabel(r'$\frac{D-B}{\sqrt{B}}$', fontsize=14.5)
plt.title("Sensitivity vs R")
plt.xticks([0.4,0.5,0.6,0.7,0.8])
yticks = [round(i,2) for i in np.linspace(0,round(maxVal,1)+0.1,11)]
plt.yticks(yticks)
plt.legend(labels, ncol = 1, loc='upper right', framealpha=0.5)
plt.grid()

# ATLAS
plt.text(0.45, 0.285, "ATLAS", fontsize=13, color='black', fontweight='bold', ha='center', style='italic')
plt.text(0.45, 0.285, "       simulation", fontsize=12, color='black')
plt.text(0.42, 0.27, r"$\sqrt{s}=13$TeV, H$\rightarrow$aa$\rightarrow$cccc", fontsize=9, color='black')
plt.text(0.42, 0.255, r"$m_{a}=20$GeV", fontsize=9, color='black')
plt.text(0.42, 0.24, "Reclustering studies", fontsize=9, color='black')

# Saving plot
plt.savefig('graph.pdf')
