#include "fastjet/ClusterSequence.hh"
#include <iostream>
#include <TROOT.h>
#include <TFile.h>
#include <TChain.h>
#include <TH1D.h>
#include <TH2F.h>
#include <TF1.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TEllipse.h>
#include <TText.h>
#include <TLatex.h>
#include <TMath.h>
#include "eventDisplay.C"
#include "DrawATLAS.C"
using namespace fastjet;
using namespace std;

vector<float> reclusterer(double R, vector<float> px, vector<float> py, vector<float> pz, vector<float> e, float leadPtCut){
    // Function for reclustering 0.4 jets into larger R jets 
    // Returns list with:
    // 0. Dijets invariant mass
    // 1. Invariant mass of leading reclustered jet
    // 2. Invariant mass of subleading reclustered jet  

    // reclustering with fastjet
    vector<fastjet::PseudoJet> inputJets;
    fastjet::JetDefinition jet_def(fastjet::antikt_algorithm, R);
    for (int k=0; k<px.size(); k++) { 
      inputJets.push_back(fastjet::PseudoJet(px.at(k), py.at(k), pz.at(k), e.at(k)));
    }

    // run the clustering, extract the jets
    fastjet::ClusterSequence cs(inputJets, jet_def);
    vector<fastjet::PseudoJet> recoReclusteredJets = fastjet::sorted_by_pt(cs.inclusive_jets());

    vector<float> pt_recl;
    vector<float> eta_recl;
    vector<float> phi_recl;
    vector<float> m_recl;

    // require at least 2 jets in event
    if (recoReclusteredJets.size()<2) {
      return {-999,-999,-999};
    }
    // require events with leading jet's pT over threshold
    if (recoReclusteredJets.at(0).pt()<leadPtCut or recoReclusteredJets.at(1).pt()<leadPtCut){
      return {-999,-999,-999};
    }

    for (int k=0; k<recoReclusteredJets.size(); k++) {
      pt_recl. push_back( recoReclusteredJets.at(k).pt()  );
      eta_recl.push_back( recoReclusteredJets.at(k).eta() );
      phi_recl.push_back( recoReclusteredJets.at(k).phi() );
      m_recl.  push_back( recoReclusteredJets.at(k).m()   );
    }

    // Calculate invariant masses
    float m2_recl = 2*pt_recl[0]*pt_recl[1]*( cosh(eta_recl[0]-eta_recl[1]) - cos(phi_recl[0]-phi_recl[1]) );
    
    //return {pt_recl, eta_recl, phi_recl};
    return {m2_recl, m_recl[0], m_recl[1]};
}

void fitter(vector<TH1D*> histos, int pTcut, int leadpTcut, const char* jetType){

  TString pdfName = Form("fits_%s.pdf",jetType);
  TCanvas* c1 = new TCanvas();
  c1->cd();

  TH1D* nominal     = histos[0];
  nominal->SetLineColor(1);
  int binMax1       = nominal->GetMaximumBin();
  float peakXCoord1 = nominal->GetXaxis()->GetBinCenter(binMax1);
  //int shift = 20;
  int shift = 30;
  TF1* gaussianFit1 = new TF1("gaussian_fit1", "gaus", peakXCoord1-shift,peakXCoord1+shift);
  nominal->Fit(gaussianFit1, "qR");
  TF1* f1     = nominal->GetFunction("gaussian_fit1");
  TF1* clone1 = new TF1();
  f1->Copy(*clone1);
  clone1->SetRange(peakXCoord1-50, peakXCoord1+50);
  clone1->SetLineStyle(2); 
  clone1->SetLineColor(kOrange+5);     
  f1->SetLineColor(kRed);        

  c1->Print(pdfName+"[");

  for (int m=1; m<histos.size(); m++){
    TLegend* leg1 = new TLegend(.9,.9,.5,.5); 

    TH1D* reclustered = histos[m];
    reclustered->SetLineColor(kGreen);        
    reclustered->GetXaxis()->SetTitle("m_{jj} [GeV]");                 

    int binMax       = reclustered->GetMaximumBin();
    float peakXCoord = reclustered->GetXaxis()->GetBinCenter(binMax);
    TF1* gaussianFit = new TF1("gaussian_fit", "gaus", peakXCoord-shift, peakXCoord+shift);
    reclustered->Fit(gaussianFit, "qR");
    TF1* f     = reclustered->GetFunction("gaussian_fit");
    TF1* clone = new TF1();
    f->Copy(*clone);
    f->SetLineColor(kBlue);       
    clone->SetRange(peakXCoord-50, peakXCoord+50);
    clone->SetLineStyle(2); 
    clone->SetLineColor(kCyan+3);     

    reclustered->GetYaxis()->SetRangeUser(0,0.018);  
    reclustered->Draw("HIST");  
    nominal    ->Draw("HIST SAME");
    f1         ->Draw("SAME");         
    f          ->Draw("SAME");        
    clone1     ->Draw("SAME");               
    clone      ->Draw("SAME");               

    leg1->AddEntry(reclustered, Form("%s, #frac{#sigma_{f}}{#mu_{f}} = %.3f",               reclustered->GetName(), f->GetParameter(2)/f->GetParameter(1) ) );
    leg1->AddEntry(f,           Form("Gaussian fit, #sigma_{fit} = %.1f, #mu_{fit} = %.0f",  f->GetParameter(2),  f->GetParameter(1) ));
    leg1->AddEntry(nominal,     Form("%s, #frac{#sigma_{f}}{#mu_{f}} = %.3f",               nominal->GetName(), f1->GetParameter(2)/f1->GetParameter(1) ) );
    leg1->AddEntry(f1,          Form("Gaussian fit, #sigma_{fit} = %.1f, #mu_{fit} = %.0f",  f1->GetParameter(2), f1->GetParameter(1) ));

    leg1->Draw("SAME");

    float xcoord = 0.14;
    float ycoord = 0.84;

    DrawATLAS(xcoord,ycoord,jetType,pTcut, leadpTcut);

    gPad->SetTicks(1);
    gPad->Update();
    
    c1->Print(pdfName);
  }
  c1->Print(pdfName+"]");
}

TCanvas* plotter(vector<TH1D*>hists, int pTcut, int leadpTcut, const char* jetType){

  TString pdfName = Form("mjj_reclustered_%s.pdf",jetType);
  TCanvas* c = new TCanvas();
  c->cd();
  TLegend* leg = new TLegend(.8,.8,.6,.6);
  for (int j=0; j<hists.size(); j++){
    hists[j]->Scale(1/hists[j]->Integral("width"));
    hists[j]->SetLineColor(j+3);
    hists[j]->GetXaxis()->SetTitle("m_{jj} [GeV]");
    hists[j]->Draw("HIST SAME");
    leg->AddEntry(hists[j], hists[j]->GetName());
  }
  leg->Draw("SAME");

  float xcoord = 0.14;
  float ycoord = 0.84;
  DrawATLAS(xcoord,ycoord,jetType,pTcut, leadpTcut);
  gPad->SetTicks(1);
  gPad->Update();
  c->Print(pdfName);

  return c;
}

int main(){

  // Avoid too many printouts when printins pdfs
  gErrorIgnoreLevel = kWarning;

  //Input files

  //Metadata
  //TFile *mData = TFile::Open("/eos/user/f/fwinkel/imGoingCrazy/trees/FTAG1/user.fwinkel.mc20_13Tev_H_a20a20_4c_FTAG1_230823_hist/user.fwinkel.34485449._000001.hist-output.root");
  TFile *mData = TFile::Open("/eos/user/f/fwinkel/imGoingCrazy/trees/FTAG1/user.fwinkel.mc20_13Tev_Zmumu_Jets_FTAG1_230823_hist/zjets_metadata.root");

  //Trees 
  TChain *tree = new TChain("analysis");

  //tree->AddFile("/eos/user/f/fwinkel/imGoingCrazy/gridRun/unCutSamples/user.fwinkel.mc20_13Tev_H_a20a20_4c_noCuts_280723_ANALYSIS.root/user.fwinkel.34218180._000001.ANALYSIS.root");
  //tree->AddFile("/eos/user/f/fwinkel/imGoingCrazy/trees/user.fwinkel.mc20_13Tev_H_a20a20_4c_noCuts_160823_ANALYSIS.root/user.fwinkel.34395901._000001.ANALYSIS.root");
  //tree->AddFile("/eos/user/f/fwinkel/imGoingCrazy/trees/user.fwinkel.mc20_13Tev_Zmumu_Jets_160823_ANALYSIS.root/zjets.root");  // Z+jets background

  // FTAG
  tree->AddFile("/eos/user/f/fwinkel/imGoingCrazy/trees/FTAG1/user.fwinkel.mc20_13Tev_H_a20a20_4c_FTAG1_230823_ANALYSIS.root/user.fwinkel.34485449._000001.ANALYSIS.root");
  //tree->AddFile("/eos/user/f/fwinkel/imGoingCrazy/trees/FTAG1/user.fwinkel.mc20_13Tev_Zmumu_Jets_FTAG1_230823_ANALYSIS.root/zjets.root");


  //Declare variables
  float eventWeight = 1;
  vector<double> *caloJet_pt  = nullptr;
  vector<double> *caloJet_eta = nullptr;
  vector<double> *caloJet_phi = nullptr;
  vector<double> *caloJet_px  = nullptr;
  vector<double> *caloJet_py  = nullptr;
  vector<double> *caloJet_pz  = nullptr;
  vector<double> *caloJet_e   = nullptr;
  vector<double> *caloJet_m   = nullptr;
  vector<double> *tJet_pt     = nullptr;
  vector<double> *tJet_eta    = nullptr;
  vector<double> *tJet_phi    = nullptr;
  vector<double> *tJet_pdgid  = nullptr;
  vector<double> *tJet_px     = nullptr;
  vector<double> *tJet_py     = nullptr;
  vector<double> *tJet_pz     = nullptr;
  vector<double> *tJet_e      = nullptr;
  vector<double> *tJet_m      = nullptr;

  //Point them to tree variables
  tree->SetBranchStatus("*",0);
  tree->SetBranchAddress("MCEventWeight", &eventWeight );
  tree->SetBranchAddress("CaloJet_pt",    &caloJet_pt );    
  tree->SetBranchAddress("CaloJet_eta",   &caloJet_eta);
  tree->SetBranchAddress("CaloJet_phi",   &caloJet_phi);
  tree->SetBranchAddress("CaloJet_px",    &caloJet_px );    
  tree->SetBranchAddress("CaloJet_py",    &caloJet_py );    
  tree->SetBranchAddress("CaloJet_pz",    &caloJet_pz );    
  tree->SetBranchAddress("CaloJet_e",     &caloJet_e );    
  tree->SetBranchAddress("CaloJet_m",     &caloJet_m );    
  tree->SetBranchAddress("TJet_pt",       &tJet_pt );
  tree->SetBranchAddress("TJet_eta",      &tJet_eta);
  tree->SetBranchAddress("TJet_phi",      &tJet_phi);
  tree->SetBranchAddress("TJet_PDGID",    &tJet_pdgid);
  tree->SetBranchAddress("TJet_px",       &tJet_px );    
  tree->SetBranchAddress("TJet_py",       &tJet_py );    
  tree->SetBranchAddress("TJet_pz",       &tJet_pz );    
  tree->SetBranchAddress("TJet_e",        &tJet_e );    
  tree->SetBranchAddress("TJet_m",        &tJet_m );    

  // Book histograms
  TH1D* hist_nominal = new TH1D("R=0.4", "Di-Charm jet invariant mass", 150, 0, 300);
  TH1D* hist_recl5   = new TH1D("R=0.5", "Di-Charm jet invariant mass", 150, 0, 300);
  TH1D* hist_recl6   = new TH1D("R=0.6", "Di-Charm jet invariant mass", 150, 0, 300);
  TH1D* hist_recl7   = new TH1D("R=0.7", "Di-Charm jet invariant mass", 150, 0, 300);
  TH1D* hist_recl8   = new TH1D("R=0.8", "Di-Charm jet invariant mass", 150, 0, 300);

  TH1D* truth_hist_nominal = new TH1D("R=0.4 (truth)", "Di-Charm jet invariant mass", 150, 0, 300);
  TH1D* truth_hist_recl5   = new TH1D("R=0.5 (truth)", "Di-Charm jet invariant mass", 150, 0, 300);
  TH1D* truth_hist_recl6   = new TH1D("R=0.6 (truth)", "Di-Charm jet invariant mass", 150, 0, 300);
  TH1D* truth_hist_recl7   = new TH1D("R=0.7 (truth)", "Di-Charm jet invariant mass", 150, 0, 300);
  TH1D* truth_hist_recl8   = new TH1D("R=0.8 (truth)", "Di-Charm jet invariant mass", 150, 0, 300);

  TH1D* leadJetMass = new TH1D("leadingJetMass", "Di-Charm jet mass", 100, 0, 100);
  TH1D* sublJetMass = new TH1D("subleadJetMass", "Di-Charm jet mass", 100, 0, 100);

  // Define vector of histograms to make plotting and fitting easier
  vector<TH1D*> hists       = {
                                hist_nominal,
                                hist_recl5,  
                                hist_recl6,  
                                hist_recl7,  
                                hist_recl8  
                              };

  vector<TH1D*> truth_hists = {
                                truth_hist_nominal,
                                truth_hist_recl5,  
                                truth_hist_recl6,  
                                truth_hist_recl7,  
                                truth_hist_recl8  
                              };


  // Displayer
  vector<vector<vector<float>>> eventsToDisplay;

  // Analysis parameters
  float m_jetPtCut      = 20; 
  float m_leadJetsPtCut = 40;

  // Counter
  int count = 0;

  // Get entries in tree 
  int nEntries = tree->GetEntries(); 
  cout<<"NEvents = "<<nEntries<<endl;

  // sum of weights
  float sumOfWeights = 0;

  //Main loop
  for (int i=0; i<nEntries; i++){
    tree->GetEntry(i);
   
    // Skip null entries ( not very likely, but can happen )
    if (tree->GetEntry(i)==0){continue;}

    // Event printout
    if (count%1000==0){ cout<<"---Entry "<<i<<endl; }

    count+=1;
   
    // Require at least two jets, reco and truth ( There is no cut in truth jets in the ntuple generation. For reco jets this is unnecesary in priciple )
    if (caloJet_pt->size()<2 or tJet_pt->size()<2){ continue; }

    // Calo jet variables
    vector<float> pt;
    vector<float> eta;
    vector<float> phi;
    vector<float> px;
    vector<float> py;
    vector<float> pz;
    vector<float> e;
    vector<float> m;

    // Truth jet variables
    vector<float> truth_pt;
    vector<float> truth_eta;
    vector<float> truth_phi;
    vector<float> truth_px;
    vector<float> truth_py;
    vector<float> truth_pz;
    vector<float> truth_e;
    vector<float> truth_m;

    // pT and eta cuts
    for (int j=0; j<caloJet_pt->size(); j++) {
      if(caloJet_pt->at(j)>m_jetPtCut and abs(caloJet_eta->at(j))<2.5) {
        pt. push_back(caloJet_pt ->at(j)); 
        eta.push_back(caloJet_eta->at(j)); 
        phi.push_back(caloJet_phi->at(j)); 
        px. push_back(caloJet_px ->at(j)); 
        py .push_back(caloJet_py ->at(j)); 
        pz .push_back(caloJet_pz ->at(j)); 
        e  .push_back(caloJet_e  ->at(j)); 
        m  .push_back(caloJet_m  ->at(j)); 
      } 
    }

    // still require at least two jets
    if (pt.size()<2){ continue; }

    ///////////////////////////////////////////////////////////
    // Recluster reco and calculate mjj for reclustered jets //
    ///////////////////////////////////////////////////////////

    // Call reclusterer function, 1st arg is mjj, 2nd is leading jet mass, 3rd is subleadingjet mass
    float m2_05 = reclusterer(0.5, px, py, pz, e, m_leadJetsPtCut)[0];
    float m2_06 = reclusterer(0.6, px, py, pz, e, m_leadJetsPtCut)[0];
    float m2_07 = reclusterer(0.7, px, py, pz, e, m_leadJetsPtCut)[0];
    float m2_08 = reclusterer(0.8, px, py, pz, e, m_leadJetsPtCut)[0];

    // Skip 1 jet events, this shouldn't be needed 
    //if (m2_05<1 or m2_05==-999){continue;}
    //if (m2_06<1 or m2_06==-999){continue;}
    //if (m2_07<1 or m2_07==-999){continue;}
    //if (m2_08<1 or m2_08==-999){continue;}

    /////////////////////////////////////////
    // Now apply cuts to nominal size jets //
    ///////////////////////////////////////// 

    // Leading jets pT cut
    if (pt.at(0)<m_leadJetsPtCut and pt.at(1)<m_leadJetsPtCut){ continue; } 

    // Calculate invariant masses
    float m2 = 2*pt[0]*pt[1]*( cosh(eta[0]-eta[1]) - cos(phi[0]-phi[1]) );

    // Fill histograms
    hist_nominal->Fill(sqrt(m2)   , eventWeight);
    hist_recl5  ->Fill(sqrt(m2_05), eventWeight);
    hist_recl6  ->Fill(sqrt(m2_06), eventWeight);
    hist_recl7  ->Fill(sqrt(m2_07), eventWeight);
    hist_recl8  ->Fill(sqrt(m2_08), eventWeight);

    /////////////////////////////////////////////////////////
    // Now fill truth info of events that passed reco cuts //
    ///////////////////////////////////////////////////////// 

    for (int j=0; j<tJet_pt->size(); j++) {
      truth_pt. push_back(tJet_pt ->at(j)); 
      truth_eta.push_back(tJet_eta->at(j)); 
      truth_phi.push_back(tJet_phi->at(j)); 
      truth_px. push_back(tJet_px ->at(j)); 
      truth_py .push_back(tJet_py ->at(j)); 
      truth_pz .push_back(tJet_pz ->at(j)); 
      truth_e  .push_back(tJet_e  ->at(j)); 
      truth_m  .push_back(tJet_m  ->at(j)); 
    }

    // Calculate (truth) invariant masses
    float truth_m2 = 2*truth_pt[0]*truth_pt[1]*( cosh(truth_eta[0]-truth_eta[1]) - cos(truth_phi[0]-truth_phi[1]) );

    // Call reclusterer function, 1st arg is mjj, 2nd is leading let mass, 3rd is subleadingjet mass
    float truth_m2_05 = reclusterer(0.5, truth_px, truth_py, truth_pz, truth_e, m_leadJetsPtCut)[0];
    float truth_m2_06 = reclusterer(0.6, truth_px, truth_py, truth_pz, truth_e, m_leadJetsPtCut)[0];
    float truth_m2_07 = reclusterer(0.7, truth_px, truth_py, truth_pz, truth_e, m_leadJetsPtCut)[0];
    float truth_m2_08 = reclusterer(0.8, truth_px, truth_py, truth_pz, truth_e, m_leadJetsPtCut)[0];

    // Fill histograms
    truth_hist_nominal->Fill( sqrt(truth_m2   ), eventWeight );
    truth_hist_recl5  ->Fill( sqrt(truth_m2_05), eventWeight );
    truth_hist_recl6  ->Fill( sqrt(truth_m2_06), eventWeight );
    truth_hist_recl7  ->Fill( sqrt(truth_m2_07), eventWeight );
    truth_hist_recl8  ->Fill( sqrt(truth_m2_08), eventWeight );

    sumOfWeights+=eventWeight;

  }

  /////////////////////////////////////
  //        Plot and fit             //
  /////////////////////////////////////

  // Call event display if necessary
  //eventDisplay(eventsToDisplay);

  // Call plotter and fitter, currently only fitting reco
  plotter(hists, m_jetPtCut, m_leadJetsPtCut, "AntiKt4EMPFlow");
  fitter( hists, m_jetPtCut, m_leadJetsPtCut, "AntiKt4EMPFlow");

  plotter(truth_hists, m_jetPtCut, m_leadJetsPtCut, "AntiKt4Truth");
  //fitter( truth_hists, m_jetPtCut, m_leadJetsPtCut, "AntiKt4Truth");

  ///////////////////////////////////// 
  // Write histograms in output file //
  /////////////////////////////////////

  // Get Metadata histogram
  TH1F *metaDataHist = (TH1F*)mData->Get("MetaData_SumW");
  //mData->Close();

  //TFile* outFile = new TFile("backgr_invariantMassHistos.root", "RECREATE");
  TFile* outFile = new TFile("signal_invariantMassHistos.root", "RECREATE");
  for (int h=0; h<hists.size();       h++){hists[h]      ->Write();}
  for (int h=0; h<truth_hists.size(); h++){truth_hists[h]->Write();}
  metaDataHist->Write();
  
  outFile->Close();
  mData->Close();

  cout<<sumOfWeights<<endl;

  return 0;
}
