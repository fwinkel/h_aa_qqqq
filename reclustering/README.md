### analysisHistogrammer.C
This is a root macro aimed to analize the events surviving all the cuts up to JVT.
It reclusters 0.4 jets into larger R jets, and plots the dijet invariant mass.
It needs to be run where fastjet is compiled (to be improved...). For now, to compile and run do:
```
g++ analysisHistogrammer.C $(root-config --cflags --glibs) -o analysisHistogrammer `fastjet-install/bin/fastjet-config --cxxflags --libs --plugins`
./analysisHistogrammer

```
### backgroundFit.py
python script for studying sensitivity vs jet radius for different cross sections.
Takes signal and background outputs from analysisHistogrammer, fits the background for different signals cross sections and does some plots.

