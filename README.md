# $H \rightarrow aa \rightarrow qqqq$ 

Welcome! This is the brand new H->aa, a->qq analysis repository. Currently under construction.

We are looking for a scalar BSM particle coming from a Higgs decay, in a process with a final state of two leptons and two jets. 

More precisely, the process we are looking for is: 

<p align="center">
<img src="diagram.png" alt="Raspberry pi" style="width:20%; border:0;">
</p>


The repository is organized in four (4) different directories/sections:

# [nTupleMaker](https://gitlab.cern.ch/fwinkel/h_aa_qqqq/-/tree/master/nTupleMaker)

This is the section of the framework that generates ntuples from DAODs.
The corresponding directory contains instructions for setup, local run and grid run.

# [gnnTagger](https://gitlab.cern.ch/fwinkel/h_aa_qqqq/-/tree/master/gnnTagger)
This is the section of the framework that performs the classification of dicharm jets vs QCD jets using a GNN.
Contains root macro that converts nTupleMaker's output to GNNs input

# [reclustering](https://gitlab.cern.ch/fwinkel/h_aa_qqqq/-/tree/master/reclustering) 
Section aimed to perform reclustering studies. Larger R jets made by reclustering 0.4 jets may benefit our (and potentially others) analysi(e)s

# [Useful scripts](https://gitlab.cern.ch/fwinkel/h_aa_qqqq/-/tree/master/usefulScripts)

This directory, as it's name points out, contains some scripts that are useful for our analysis.

# **TODO's:**
Last update: Sanha Cheong, 2024/02/05

## **nTupleMaker**
  1. Organize CP tools. Wrap them into `AnaToolHandle` or pakcage them into a custom class like `TrackTool`.

  2. Properly implement `PileupReweightingTool`. Currently, the package is included correctly, but need to implement its proper initialization / configuration.

  3. Validate Muon CP tools. All the Muon CP codes are properly implemented, but their results could not be validated because some of the muon CP tools are run "period" dependent, so they do not work properly without the PRW tool applied. Make sure that these work without error, after having completed 2.

  4. Fetching jet-track variables. The selected jet-tracks are now easy to loop through, but the current code doesn't actually fetch the track variables into the analysis TTree.

  5. Implement systematics tools.

## **Custom Tagger from Salt**
  5. The tagger output scores are still "mis-labeled" somehow. There might be class labels that are incorrectly defined, or auto-remapping that is done in Python (Salt) but not imported properly into `network.onnx` files. Look into this and investigate.
---

###
*No llores por las heridas que no paran de sangrar, no llores por mi Argentina, te quiero cada dia mas* 
